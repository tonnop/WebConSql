<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%@ page import="com.login.action.ShowUserAction"%>
<%@ page import="com.login.model.ImgModel"%>
<%@ page import="java.io.File"%>
<%@ page import="java.sql.Blob"%>

<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
	String resultDelete = String.valueOf(request
			.getAttribute("resultDelete"));
	String resultEdit = String.valueOf(request
			.getAttribute("resultEdit"));
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<!-- jQuery library -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!-- Popper JS -->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<!-- Latest compiled JavaScript -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>


<head>
<base href="<%=basePath%>">

<title>Show Report</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

<style>
.buttonHead {
	padding-top: 15px;
}
</style>

</head>

<body>



	<div class="container" style="background-color: #FDF7F7;">

		<nav class="navbar navbar-expand-lg navbar-light "
			style="background-color: #e3f2fd;"> <a class="navbar-brand"
			href="ShowUser">Dota 2</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarSupportedContent"
			aria-controls="navbarSupportedContent" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active"><a class="nav-link" href="ShowUser">Home
						<span class="sr-only">(current)</span>
				</a></li>
				<li class="nav-item"><a class="nav-link" href="PageAddUser.jsp">Insert</a></li>
				<li class="nav-item"><a class="nav-link"
					href="CreateReport.jsp">Insert Report</a></li>
				<li class="nav-item"><a class="nav-link" href="ShowReport">Show
						Report</a></li>

			</ul>
			<%
				String sessionid = null;
				if (session.getAttribute("sessionid") == null) {
			%>
			<button type="button" class="btn btn-outline-success my-2 my-sm-0"
				data-toggle="modal" data-target="#exampleModal">Login</button>
			<%
				} else {
					sessionid = (String) session.getAttribute("sessionid");

					String userName = null;
					String sessionID = null;
					Cookie[] cookies = request.getCookies();
					if (cookies != null) {
						for (Cookie cookie : cookies) {
							if (cookie.getName().equals("sessionid"))
								userName = cookie.getValue();
							if (cookie.getName().equals("JSESSIONID"))
								sessionID = cookie.getValue();
							else {
								sessionID = session.getId();
							}
							//System.out.println("cookie.getName()" + cookie.getName());
							//System.out.println("cookie.getValue()" + cookie.getValue());
						}
					}
			%>
			<div class="dropdown">
				<button class="btn btn-secondary dropdown-toggle" type="button"
					id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
					aria-expanded="false"><%=sessionid%></button>
				<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
					<a class="dropdown-item" href="#"> Profile </a><a
						class="dropdown-item" href="Logout"> Sing Out </a>
				</div>
			</div>

			<%
				}
			%>

			<!-- Modal -->
			<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
				aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title text-muted" id="exampleModalLabel">
								Login.</h5>

						</div>
						<br>
						<div class="container">
							<form action="Login" method="post">
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text">Username</span>
									</div>
									<input type="text" class="form-control" name="username"
										placeholder="Username" required>
								</div>
								<p>
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text">Password&nbsp;</span>
									</div>
									<input type="password" class="form-control" name="password"
										placeholder="Password" required>

								</div>
								<div align="right">
									<br>
									<button type="submit" class="btn btn-outline-success">Login</button>
									<button type="button" class="btn btn-outline-danger"
										data-dismiss="modal">Close</button>
								</div>
							</form>
						</div>


					</div>
				</div>
			</div>

		</div>
		</nav>

		<div id="myCarousel" class="carousel slide" data-ride="carousel">
			<div class="item active">
				<img
					src="FileServlet?pathImage=E:\PROJECT-MYECLIPSE\WebConSql\slide\tabbar.jpg"
					class="rounded" alt="Cinque Terre" style="width:100%; height:20%"
					height="100">
				<div class="carousel-caption" align="center">
					<h3 class="font-italic">Show Report</h3>
				</div>
			</div>
		</div>

		<div class="row">

			<div class="col-1"></div>
			<div class="col">

				<br>
				<!-- Message Alert!! -->
				<%
					if (resultDelete == "true") {
				%>
				<div class="alert alert-success alert-dismissible">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Success!</strong> Your file was successfully delete!!
				</div>
				<%
					} else if (resultDelete == "false") {
				%>
				<div class="alert alert-danger alert-dismissible">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Error!</strong> There is error delete file. Please try
					again.
				</div>
				<%
					}
				%>
				<%
					if (resultEdit == "true") {
				%>

				<div class="alert alert-success alert-dismissible">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Success!</strong> Your file edit was successfully!!
				</div>
				<%
					}
				%>
				<!-- End Message Alert!! -->

				<div class="accordion" id="accordion" role="tablist">
					<%
						if (request.getAttribute("listModel") != null) {
							int i = 0, reStatus;//Loop Head
							int n = 0, infid = 0;//Check Head Repeatedly
							String reportName = null;
							String reportDesc = null;
							String reportStatus = null;
							String titleImg = null;
							String statusImg = null;
							String descImg = null;
							String imgPath = null;

							List<ImgModel> listModel = (ArrayList<ImgModel>) request
									.getAttribute("listModel");

							if (listModel.size() > 0) {
								while (i < listModel.size()) {
									if (i != 0) {
										n = i - 1;
									}
									if ((listModel.get(i).getInfId() != listModel.get(n)
											.getInfId()) || i == 0) {
										ImgModel imgHeadModel = listModel.get(i);
										reportName = imgHeadModel.getReportName();
										reportDesc = imgHeadModel.getReportDesc();
										reStatus = imgHeadModel.getReportStatus();

										switch (reStatus) {
										case 0:
											reportStatus = "Non Active";
											break;
										case 1:
											reportStatus = "Active";
											break;

										}

										//test use listdetail
										infid = imgHeadModel.getInfId();
					%>



					<div class="card">
						<div class="card-header btn btn-outline-secondary"
							id="heading<%=i%>" data-toggle="collapse"
							data-target="#collapse<%=i%>" aria-expanded="true"
							aria-controls="collapse<%=i%>">
							<div class="row">
								<div class="col-10 col-form-label">

									<div class="form-group row" align="left">
										<label for="staticEmail" class="col-sm-3 col-form-label">Report
											Name</label> <label class="col-sm-9 col-form-label"><%=reportName%></label>
									</div>
									<div class="form-group row" align="left">
										<div for="staticEmail" class="col-sm-3 col-form-label"
											align="left">Report Description</div>
										<div class="col-sm-9 col-form-label"><%=reportDesc%></div>
									</div>
									<div class="form-group row" align="left">
										<label for="staticEmail" class="col-sm-3 col-form-label"
											align="left">Report Status</label> <label
											class="col-sm-9 col-form-label"><%=reportStatus%></label>
									</div>
								</div>

								<div class="col col-form-label buttonHead" align="center">

									<a href="EditReport?infid=<%=infid%>"
										class="btn btn-outline-warning btn-lg btn-block"> Edit </a>
									&nbsp;

									<button type="button"
										class="btn btn-outline-danger btn-lg btn-block"
										data-toggle="modal" data-target="#exampleModal<%=infid%>">Delete</button>

									<!-- Modal -->
									<div class="modal fade" id="exampleModal<%=infid%>"
										tabindex="-1" role="dialog"
										aria-labelledby="exampleModalLabel<%=infid%>"
										aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<h5 class="modal-title text-muted" id="exampleModalLabel">
														Delete
														<%=reportName%>. Are you sure?
													</h5>

												</div>
												<div class="modal-body text-muted">You won't be able
													to revert this!</div>
												<div class="modal-footer">
													<a href="DeleteReport?infid=<%=infid%>"
														class="btn btn-outline-danger">Delete</a>
													<button type="button" class="btn btn-outline-secondary"
														data-dismiss="modal">Close</button>
												</div>
											</div>
										</div>
									</div>

								</div>
							</div>
						</div>

						<%
							if (reStatus == 1) {
												int d = 0; //Loop detail
												while (d < listModel.size()) {
													ImgModel imgDetaliModel = listModel.get(d);

													if ((infid == imgDetaliModel
															.getListDetail().get(d)
															.getRefInfId())) {

														titleImg = imgDetaliModel
																.getListDetail().get(d)
																.getTitleImg();
														int imgstatus = imgDetaliModel
																.getListDetail().get(d)
																.getStatusImg();

														switch (imgstatus) {
														case 0:
															statusImg = "Non Active";
															break;
														case 1:
															statusImg = "Active";
															break;

														}

														descImg = imgDetaliModel
																.getListDetail().get(d)
																.getDescImg();
														imgPath = imgDetaliModel
																.getListDetail().get(d)
																.getImgPath();
						%>

						<div id="collapse<%=i%>" class="collapse"
							aria-labelledby="heading<%=i%>" data-parent="#accordion">

							<div class="card-body">
								<div class="container row">
									<div class="col">

										<!-- Loop Detail -->
										<div class="form-group row" align="left">
											<label class="col-sm-3 col-form-label">Title Img</label> <label
												class="col-sm-9 col-form-label"><%=titleImg%></label>
										</div>

										<div class="form-group row" align="left">
											<label class="col-sm-3 col-form-label">Status Image</label> <label
												class="col-sm-9 col-form-label"><%=statusImg%></label>
										</div>

										<div class="form-group row" align="left">
											<label class="col-sm-3 col-form-label">Desc Img</label> <label
												class="col-sm-9 col-form-label"><%=descImg%></label>
										</div>

										<div class="form-group" align="center">
											<%
												if (imgstatus == 1) {
																				if (imgPath.equals("")) {
											%>
											<img
												src="FileServlet?pathImage=E:\PROJECT-MYECLIPSE\WebConSql\slide\No_Image_Available.jpg"
												class="rounded" alt="Cinque Terre" align="middle"
												width="300" height="200" />

											<%
												} else {
											%>

											<img src="FileServlet?pathImage=<%=imgPath%>" class="rounded"
												alt="Cinque Terre" align="middle" width="300" height="200" />

											<%
												}//End else check Img Path

																			}
											%>
										</div>
										<div class="form-group" align="center">- - - - - - - - -
											- - -</div>
										<!-- End Loop Detail -->
									</div>

								</div>
							</div>
						</div>
						<%
							}//End if detail loop
													d++;
												}//End while detail loop
											}//End if check Active
						%>

					</div>
					<%
						}//End if head loop
					%>



					<!-- End Loop Head -->

					<%
						i++;
								}//End while
							}//End if listModel.size()

						}//End if listModel != null

						//https://coreui.io/docs/components/bootstrap-collapse/
						//Accordion Example
					%>
					<br>
				</div>
			</div>
			<div class="col-1"></div>

		</div>
	</div>
</body>

</html>
