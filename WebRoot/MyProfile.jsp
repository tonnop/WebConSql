<%@page import="com.login.model.UserModel"%>
<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";

	String resultDelete = "false";
	UserModel userModel = (UserModel) request.getAttribute("userModel");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<!-- jQuery library -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!-- Popper JS -->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<!-- Latest compiled JavaScript -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

<head>
<base href="<%=basePath%>">

<title>My JSP 'MyProfile.jsp' starting page</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

</head>

<body>
<body>
	<div class="container" style="background-color: #FDF7F7;">

		<!-- Message Alert!! -->
		<%
			if (resultDelete == "true") {
		%>
		<div class="alert alert-success alert-dismissible">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>Success!</strong> Your file was successfully delete!!
		</div>
		<%
			}
		%>
		<nav class="navbar navbar-expand-lg navbar-light "
			style="background-color: #e3f2fd;"> <a class="navbar-brand"
			href="ShowUser">Dota 2</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarSupportedContent"
			aria-controls="navbarSupportedContent" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active"><a class="nav-link" href="ShowUser">Home
						<span class="sr-only">(current)</span>
				</a></li>
				<li class="nav-item"><a class="nav-link" href="PageAddUser.jsp">Insert</a></li>
				<li class="nav-item"><a class="nav-link"
					href="CreateReport.jsp">Insert Report</a></li>
				<li class="nav-item"><a class="nav-link" href="ShowReport">Show
						Report</a></li>

			</ul>
			<%
				String sessionid = null;
				if (session.getAttribute("sessionid") == null) {
			%>
			<button type="button" class="btn btn-outline-success my-2 my-sm-0"
				data-toggle="modal" data-target="#exampleModal">Login</button>
			<%
				} else {
					sessionid = (String) session.getAttribute("sessionid");

					String userName = null;
					String sessionID = null;
					Cookie[] cookies = request.getCookies();
					if (cookies != null) {
						for (Cookie cookie : cookies) {
							if (cookie.getName().equals("sessionid"))
								userName = cookie.getValue();
							if (cookie.getName().equals("JSESSIONID"))
								sessionID = cookie.getValue();
							else {
								sessionID = session.getId();
							}
							//System.out.println("cookie.getName()" + cookie.getName());
							//System.out.println("cookie.getValue()" + cookie.getValue());
						}
					}
			%>
			<div class="dropdown">
				<button class="btn btn-secondary dropdown-toggle" type="button"
					id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
					aria-expanded="false"><%=sessionid%></button>
				<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
					<!-- <a class="dropdown-item" href="response.encodeURL("MyProfile")"> Profile </a> -->
					<a class="dropdown-item" href="MyProfile"> Profile </a><a
						class="dropdown-item" href="Logout"> Sing Out </a>
				</div>
			</div>

			<%
				}
			%>

			<!-- Modal -->
			<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
				aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title text-muted" id="exampleModalLabel">
								Login.</h5>

						</div>
						<br>
						<div class="container">
							<div class="input-group-prepend">
								<span class="input-group-text">Username</span>
							</div>
							<form action="Login" method="post">
								<div class="input-group">

									<input type="text" class="form-control" name="username"
										placeholder="Username" required>
								</div>
								<p>
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text">Password&nbsp;</span>
									</div>
									<input type="password" class="form-control" name="password"
										placeholder="Password" required>

								</div>
								<div align="right">
									<br>
									<button type="submit" class="btn btn-outline-success">Login</button>
									<button type="button" class="btn btn-outline-danger"
										data-dismiss="modal">Close</button>
								</div>
							</form>
						</div>


					</div>
				</div>
			</div>

		</div>
		</nav>

	</div>
	<!-- End Navbar -->

	<div class="container" style="background-color: #FDF7F7;">

		<div align="center">
			<br>
			<h1>
				<span class="badge badge-secondary">My Profile</span>
			</h1>
			<br>
			<%
				if (userModel.getPicPath() != null) {
			%>
			<img src="FileServlet?pathImage=<%=userModel.getPicPath()%>"
				class="rounded-circle" alt="Cinque Terre" align="middle" width="250"
				height="250">
			<%
				} else {
			%>
			<img
				src="FileServlet?pathImage=E:\PROJECT-MYECLIPSE\WebConSql\slide\No_Image_Available.jpg"
				class="rounded-circle" alt="Cinque Terre" align="middle" width="250"
				height="250">
			<%
				}
			%>
		</div>
		
		

	</div>
	<!-- End Container color -->
</body>

</html>
