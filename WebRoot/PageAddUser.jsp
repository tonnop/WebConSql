<%@page import="com.login.model.UserModel"%>
<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";

	String resultAdd = String
			.valueOf(request.getAttribute("resultAdd"));

	UserModel userModel = (UserModel) request.getAttribute("userModel");

	String id = "";
	String pass = "";
	String tel = "";
	String sex = "";
	String age = "";
	String address = "";
	String oldphoto = "";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<!-- jQuery library -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!-- Popper JS -->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<!-- Latest compiled JavaScript -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>


<head>
<base href="<%=basePath%>">

<title>My JSP 'PageAddUser.jsp' starting page</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
<script type="text/javascript">
	function fncReset(){
	document.form1.id.value = "";
	document.form1.pass.value = "";
	document.form1.tel.value = "";
	document.form1.age.value = "";
	document.form1.address.value = "";
	document.form1.sex.value = "0";
	document.form1.photo.value = "";
	}
</script>
</head>

<body>
	<div class="container" style="background-color: #FDF7F7;">
		<nav class="navbar navbar-expand-lg navbar-light "
			style="background-color: #e3f2fd;"> <a class="navbar-brand"
			href="ShowUser">Dota 2</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarSupportedContent"
			aria-controls="navbarSupportedContent" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active"><a class="nav-link" href="ShowUser">Home
						<span class="sr-only">(current)</span>
				</a></li>
				<li class="nav-item"><a class="nav-link" href="PageAddUser.jsp">Insert</a></li>
				<li class="nav-item"><a class="nav-link"
					href="CreateReport.jsp">Insert Report</a></li>
				<li class="nav-item"><a class="nav-link" href="ShowReport">Show
						Report</a></li>

			</ul>
			<%
				String sessionid = null;
				if (session.getAttribute("sessionid") == null) {
			%>
			<button type="button" class="btn btn-outline-success my-2 my-sm-0"
				data-toggle="modal" data-target="#exampleModal">Login</button>
			<%
				} else {
					sessionid = (String) session.getAttribute("sessionid");

					String userName = null;
					String sessionID = null;
					Cookie[] cookies = request.getCookies();
					if (cookies != null) {
						for (Cookie cookie : cookies) {
							if (cookie.getName().equals("sessionid"))
								userName = cookie.getValue();
							if (cookie.getName().equals("JSESSIONID"))
								sessionID = cookie.getValue();
							else {
								sessionID = session.getId();
							}
							//System.out.println("cookie.getName()" + cookie.getName());
							//System.out.println("cookie.getValue()" + cookie.getValue());
						}
					}
			%>
			<div class="dropdown">
				<button class="btn btn-secondary dropdown-toggle" type="button"
					id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
					aria-expanded="false"><%=sessionid%></button>
				<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
					<a class="dropdown-item" href="#"> Profile </a><a
						class="dropdown-item" href="Logout"> Sing Out </a>
				</div>
			</div>

			<%
				}
			%>

			<!-- Modal -->
			<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
				aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title text-muted" id="exampleModalLabel">
								Login.</h5>

						</div>
						<br>
						<div class="container">
							<form action="Login" method="post">
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text">Username</span>
									</div>
									<input type="text" class="form-control" name="username"
										placeholder="Username" required>
								</div>
								<p>
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text">Password&nbsp;</span>
									</div>
									<input type="password" class="form-control" name="password"
										placeholder="Password" required>

								</div>
								<div align="right">
									<br>
									<button type="submit" class="btn btn-outline-success">Login</button>
									<button type="button" class="btn btn-outline-danger"
										data-dismiss="modal">Close</button>
								</div>
							</form>
						</div>


					</div>
				</div>
			</div>


		</div>
		</nav>

		<div id="myCarousel" class="carousel slide" data-ride="carousel">
			<div class="item active">
				<img
					src="FileServlet?pathImage=E:\PROJECT-MYECLIPSE\WebConSql\slide\tabbar.jpg"
					class="rounded" alt="Cinque Terre" style="width:100%; height:20%"
					height="100">
				<div class="carousel-caption" align="center">
					<h3 class="font-italic">Insert Data</h3>
				</div>
			</div>
		</div>
		<br>
		<!-- Message Alert!! -->
		<%
			if (resultAdd == "false") {
		%>

		<div class="alert alert-danger alert-dismissible">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>Error!</strong> There is error save file. Please try again.
		</div>
		<%
			}
		%>
		<!-- End Message Alert!! -->
		<center>
			<form name="form1" method="POST" action='AddUser' enctype="multipart/form-data">
				<!-- enctype="multipart/form-data" -->
		</center>

		<div class="row">

			<div class="col-2"></div>
			<div class="col">
				<%
					if (userModel != null) {
						id = userModel.getID();
						pass = userModel.getPass();
						tel = userModel.getTel();
						sex = userModel.getSex();
						age = userModel.getAge();
						address = userModel.getAddress();
				%>
				<div class="form-group">
					<label>ID :</label> <input type="text" name="id"
						class="form-control" placeholder="Enter ID" value="<%=id%>">
				</div>
				<div class="form-group">
					<label for="pwd">Password :</label><input type="password"
						name="pass" class="form-control" placeholder="Enter password">
				</div>
				<div class="form-group">
					<label for="pwd">Telephone :</label><input type="text" name="tel"
						class="form-control" placeholder="Enter Telephone" value="<%=tel%>">
				</div>

				<%
					if (sex.equals("1")) {
				%>

				<div class="form-group">
					<label for="pwd">Sex :</label> <select class="form-control"
						name="sex">
						<option value="0">Enter Sex</option>
						<option value="1" selected="selected">Man</option>
						<option value="2">Women</option>
					</select>
				</div>
				<%
					} else if (sex.equals("2")) {
				%>
				<div class="form-group">
					<label for="pwd">Sex :</label> <select class="form-control"
						name="sex">
						<option value="0">Enter Sex</option>
						<option value="1">Man</option>
						<option value="2" selected="selected">Women</option>
					</select>
				</div>
				<%
					} else {
				%>
				<div class="form-group">
					<label for="pwd">Sex :</label> <select class="form-control"
						name="sex">
						<option value="0" selected="selected">Enter Sex</option>
						<option value="1">Man</option>
						<option value="2">Women</option>
					</select>
				</div>
				<%
					}
				%>

				<div class="form-group">
					<label for="pwd">Age :</label><input type="text" name="age"
						class="form-control" placeholder="Enter Age" value="<%=age%>">
				</div>
				<div class="form-group">
					<label for="pwd">Address :</label><input type="text" name="address"
						class="form-control" placeholder="Enter Address" value="<%=address%>">
				</div>

				<%
					} else {
				%>

				<div class="form-group">
					<label>ID :</label> <input type="text" name="id"
						class="form-control" placeholder="Enter ID">
				</div>
				<div class="form-group">
					<label for="pwd">Password :</label><input type="password"
						name="pass" class="form-control" placeholder="Enter password">
				</div>
				<div class="form-group">
					<label for="pwd">Telephone :</label><input type="text" name="tel"
						class="form-control" placeholder="Enter Telephone">
				</div>
				<div class="form-group">
					<label for="pwd">Sex :</label> <select class="form-control"
						name="sex">
						<option value="0" selected="selected">Enter Sex</option>
						<option value="1">Man</option>
						<option value="2">Women</option>
					</select>
				</div>
				<div class="form-group">
					<label for="pwd">Age :</label><input type="text" name="age"
						class="form-control" placeholder="Enter Age">
				</div>
				<div class="form-group">
					<label for="pwd">Address :</label><input type="text" name="address"
						class="form-control" placeholder="Enter Address">
				</div>
				<%
					}
				%>

				<div class="form-group">
					<label for="pwd">Portrait Photo :</label><input type="file"
						name="photo" class="form-control">
				</div>
				<div class="alert alert-danger" role="alert">Please upload
					only file type jpg, jpeg, png, gif and use file size less than
					40MB.</div>
			</div>
			<div class="col-2"></div>
		</div>

		<br>
		<div class="row">
			<div class="col"></div>
			<div class="col" align="center">
				<button type="submit" class="btn btn-outline-success">
					submit</button>
			</div>
			<div class="col" align="center">
				<button type="button" class="btn btn-outline-primary" onclick="fncReset();">
					Clear</button>
			</div>
			<div class="col" align="center">
				<button type="button" class="btn btn-outline-primary"
					onClick="history.go(-1);">Back</button>
			</div>
			<div class="col"></div>
		</div>
		<br>
	</div>
</body>
</html>
