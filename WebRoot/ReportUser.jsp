<%@ page import="java.sql.Blob"%>
<%@ page import="java.io.File"%>
<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%@ page import="com.login.action.ShowUserAction"%>
<%@ page import="com.login.model.UserModel"%>

<%
	String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
    String resultDelete = String.valueOf(request.getAttribute("result"));
    String resultEdit = String.valueOf(request.getAttribute("resultEdit"));
    String resultAdd = String.valueOf(request.getAttribute("resultAdd"));
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<!-- jQuery library -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!-- Popper JS -->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<!-- Latest compiled JavaScript -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

<head>
<base href="<%=basePath%>">
<title>My JSP 'ReportUser.jsp' starting page</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">

<script type="text/javascript">
	function fncReset() {
		document.form1.sid.value = "";
		document.form1.spass.value = "";
		document.form1.stel.value = "";
		document.form1.ssex.value = "";
		document.form1.sage.value = "";
		document.form1.saddress.value = "";
	}
</script>
</head>

<body>
	<div class="container" style="background-color: #FDF7F7;">

		<!-- Message Alert!! -->
		<%
			if (resultDelete == "true") {
		%>
		<div class="alert alert-success alert-dismissible">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>Success!</strong> Your file was successfully delete!!
		</div>
		<%
			} else if (resultDelete == "false") {
		%>
		<div class="alert alert-danger alert-dismissible">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>Error!</strong> There is error delete file. Please try again.
		</div>
		<%
			}
		%>
		<%
			if (resultEdit == "true") {
		%>

		<div class="alert alert-success alert-dismissible">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>Success!</strong> Your file edit was successfully!!
		</div>
		<%
			}
		%>
		<!-- Message Alert!! -->
		<%
			if (resultAdd == "true") {
		%>

		<div class="alert alert-success alert-dismissible">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>Success!</strong> Your file add was successfully!!
		</div>
		<%
			}
		%>
		<!-- End Message Alert!! -->

		<nav class="navbar navbar-expand-lg navbar-light "
			style="background-color: #e3f2fd;"> <a class="navbar-brand"
			href="ShowUser">Dota 2</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarSupportedContent"
			aria-controls="navbarSupportedContent" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active"><a class="nav-link" href="ShowUser">Home
						<span class="sr-only">(current)</span>
				</a></li>
				<li class="nav-item"><a class="nav-link" href="PageAddUser.jsp">Insert</a></li>
				<li class="nav-item"><a class="nav-link"
					href="CreateReport.jsp">Insert Report</a></li>
				<li class="nav-item"><a class="nav-link" href="ShowReport">Show
						Report</a></li>

			</ul>
			<%
				String sessionid = null;
				if (session.getAttribute("sessionid") == null) {
			%>
			<button type="button" class="btn btn-outline-success my-2 my-sm-0"
				data-toggle="modal" data-target="#exampleModal">Login</button>
			<%
				} else {
					sessionid = (String) session.getAttribute("sessionid");

					String userName = null;
					String sessionID = null;
					Cookie[] cookies = request.getCookies();
					if (cookies != null) {
						for (Cookie cookie : cookies) {
							if (cookie.getName().equals("sessionid"))
								userName = cookie.getValue();
							if (cookie.getName().equals("JSESSIONID"))
								sessionID = cookie.getValue();
							else {
								sessionID = session.getId();
							}
							//System.out.println("cookie.getName()" + cookie.getName());
							//System.out.println("cookie.getValue()" + cookie.getValue());
						}
					}
			%>
			<div class="dropdown">
				<button class="btn btn-secondary dropdown-toggle" type="button"
					id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
					aria-expanded="false"><%=sessionid%></button>
				<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
				<!-- <a class="dropdown-item" href="response.encodeURL("MyProfile")"> Profile </a> -->
					<a class="dropdown-item" href="MyProfile"> Profile </a><a
						class="dropdown-item" href="Logout"> Sing Out </a>
				</div>
			</div>

			<%
				}
			%>

			<!-- Modal -->
			<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
				aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title text-muted" id="exampleModalLabel">
								Login.</h5>

						</div>
						<br>
						<div class="container">
							<form action="Login" method="post">
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text">Username</span>
									</div>
									<input type="text" class="form-control" name="username"
										placeholder="Username" required>
								</div>
								<p>
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text">Password&nbsp;</span>
									</div>
									<input type="password" class="form-control" name="password"
										placeholder="Password" required>

								</div>
								<div align="right">
									<br>
									<button type="submit" class="btn btn-outline-success">Login</button>
									<button type="button" class="btn btn-outline-danger"
										data-dismiss="modal">Close</button>
								</div>
							</form>
						</div>


					</div>
				</div>
			</div>

		</div>
		</nav>

	</div>


	<div id="carouselExampleIndicators" class="carousel slide"
		data-ride="carousel">
		<ol class="carousel-indicators">
			<li data-target="#carouselExampleIndicators" data-slide-to="0"
				class="active"></li>
			<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
			<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
		</ol>
		<div class="carousel-inner">
			<div class="carousel-item active">
				<img class="d-block w-100 h-50"
					src="FileServlet?pathImage=E:\PROJECT-MYECLIPSE\WebConSql\slide\slide-1.jpg"
					alt="First slide">
			</div>
			<div class="carousel-item">
				<img class="d-block w-100 h-50"
					src="FileServlet?pathImage=E:\PROJECT-MYECLIPSE\WebConSql\slide\slide-2.jpg"
					alt="Second slide">
			</div>
			<div class="carousel-item">
				<img class="d-block w-100 h-50"
					src="FileServlet?pathImage=E:\PROJECT-MYECLIPSE\WebConSql\slide\slide-3.jpg"
					alt="Third slide">
			</div>
		</div>
		<a class="carousel-control-prev" href="#carouselExampleIndicators"
			role="button" data-slide="prev"> <span
			class="carousel-control-prev-icon" aria-hidden="true"></span> <span
			class="sr-only">Previous</span>
		</a> <a class="carousel-control-next" href="#carouselExampleIndicators"
			role="button" data-slide="next"> <span
			class="carousel-control-next-icon" aria-hidden="true"></span> <span
			class="sr-only">Next</span>
		</a>
	</div>

	<%
		String sid = "";
		String spass = "";
		String stel = "";
		String ssex = "";
		String sage = "";
		String saddress = "";

		UserModel se = (UserModel) request.getAttribute("se");
		if (se.getID() != null) {
			sid = se.getID();
		}
		if (se.getPass() != null) {
			spass = se.getPass();
		}
		if (se.getTel() != null) {
			stel = se.getTel();
		}
		if (se.getSex() != null) {
			ssex = se.getSex();
		}
		if (se.getAge() != null) {
			sage = se.getAge();
		}
		if (se.getAddress() != null) {
			saddress = se.getAddress();
		}
	%>
	<div class="container" style="background-color: #FDF7F7;">

		<form name=form1 method="post" action="ShowUser">

			<br>

			<div class="row">
				<div class="col-4">
					ID : <input type="text" class="form-control" name="sid"
						value="<%=sid%>" />
				</div>
				<div class="col-4">
					Pass : <input type="text" class="form-control" name="spass"
						value="<%=spass%>" />
				</div>
				<div class="col-4">
					Tel : <input type="text" class="form-control" name="stel"
						value="<%=stel%>" />
				</div>
			</div>
			<p>
			<div class="row">
				<div class="col-4">
					Sex : <input type="text" class="form-control" name="ssex"
						value="<%=ssex%>" />
				</div>
				<div class="col-4">
					Age : <input type="text" class="form-control" name="sage"
						value="<%=sage%>" />
				</div>
				<div class="col-4">
					Address : <input type="text" class="form-control" name="saddress"
						value="<%=saddress%>" />
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col"></div>
				<div class="col" align="right">
					<button type="button" class="btn btn-outline-primary "
						onclick="fncReset();">Clear</button>
				</div>
				<div class="col">
					<button type="submit" class="btn btn-outline-success">Search</button>
				</div>
				<div class="col"></div>
			</div>

		</form>

		<table class="table table-striped">
			<thead>
				<tr align="center">
					<th scope="col">ID</th>
					<th scope="col">Pass</th>
					<th scope="col">Tel</th>
					<th scope="col">Sex</th>
					<th scope="col">Age</th>
					<th scope="col">Address</th>
					<th scope="col">Photo</th>
					<th scope="col">Edit</th>
					<th scope="col">Delete</th>

				</tr>
			</thead>
			<%
				if (request.getAttribute("ListUser") != null) {

					List<UserModel> ListUser = (ArrayList<UserModel>) request
							.getAttribute("ListUser");

					if (ListUser.size() > 0) {
						for (int i = 0; i < ListUser.size(); i++) {

							UserModel model = ListUser.get(i);
							String id = model.getID();
							String pass = model.getPass();
							String tel = model.getTel();
							String sex = model.getSex();
							String age = model.getAge();
							String address = model.getAddress();
							String photo = model.getPicPath();
			%>

			<tbody>
				<tr align="center">
					<th scope="row"><%=id%></th>
					<td><%=pass%></td>
					<td><%=tel%></td>
					<td>
						<%
							if (sex.equals("1")) {
						%>Man<%
							} else if (sex.equals("2")) {
						%>Women<%
							} else {
						%>Non Select<%
							}
						%>
					</td>
					<td><%=age%></td>
					<td><%=address%></td>


					<%
						if (photo.equals("")) {
					%>
					<td><img
						src="FileServlet?pathImage=E:\PROJECT-MYECLIPSE\WebConSql\slide\No_Image_Available.jpg"
						class="rounded" alt="Cinque Terre" align="middle" width="200"
						height="100"></td>
					<%
						} else {
										File imageFile = new File(photo);
					%>
					<td><img
						src="FileServlet?pathImage=<%=imageFile.getAbsolutePath()%>"
						class="rounded" alt="Cinque Terre" align="middle" width="200"
						height="100"></td>
					<%
						}// end else
					%>


					<td><a href="editUser?id=<%=id%>"
						class="btn btn-outline-warning">Edit</a></td>

					<td>


						<button type="button" class="btn btn-outline-danger"
							data-toggle="modal" data-target="#exampleModal<%=id%>">Delete</button>

						<!-- Modal -->
						<div class="modal fade" id="exampleModal<%=id%>" tabindex="-1"
							role="dialog" aria-labelledby="exampleModalLabel<%=id%>"
							aria-hidden="true">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title text-muted" id="exampleModalLabel">
											Delete
											<%=id%>. Are you sure?
										</h5>

									</div>
									<div class="modal-body text-muted">You won't be able to
										revert this!</div>
									<div class="modal-footer">
										<a href="ServletDelete?id=<%=id%>&photo=<%=photo%>"
											class="btn btn-outline-danger">Delete</a>
										<button type="button" class="btn btn-outline-secondary"
											data-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</td>
				</tr>
			</tbody>


			<%
				}//end for
					}//end if
				}//end if
				else {
					System.out.println("ListUser Null");
				}//end else
			%>
		</table>
		<br>
	</div>
</body>
</html>
