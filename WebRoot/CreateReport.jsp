<%@page import="com.login.model.ImgDetailModel"%>
<%@page import="com.login.model.ImgModel"%>
<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
	+ request.getServerName() + ":" + request.getServerPort()
	+ path + "/";
	int n = 1;
	String resultAdd = String
	.valueOf(request.getAttribute("resultAdd"));
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<!-- jQuery library -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!-- Popper JS -->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<!-- Latest compiled JavaScript -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>



<!-- add to document <head> -->
<link href="https://unpkg.com/filepond/dist/filepond.css"
	rel="stylesheet">

<!-- add before </body> -->
<script src="https://unpkg.com/filepond/dist/filepond.js"></script>



<head>
<base href="<%=basePath%>">

<title>Create Report</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

<script type="text/javascript">
	function fncReset() {

		document.form1.reportName.value = "";
		document.form1.reportDes.value = "";
		document.form1.reportStatus.value = "1";
		var title1 = document.getElementsByName("titleImg[]");
		var desc1 = document.getElementsByName("descImg[]");
		var status1 = document.getElementsByName("statusImg[]");
		var photo1 = document.getElementsByName("photo[]");

		for ( var i = 0; i < title1.length; i++) {
			title1[i].value = "";
			desc1[i].value = "";
			status1[i].value = "1";
			photo1[i].value = "";
		}
	}
</script>

</head>

<body>

	<div class="container" style="background-color: #FDF7F7;">
		<nav class="navbar navbar-expand-lg navbar-light "
			style="background-color: #e3f2fd;"> <a class="navbar-brand"
			href="ShowUser">Dota 2</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarSupportedContent"
			aria-controls="navbarSupportedContent" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active"><a class="nav-link" href="ShowUser">Home
						<span class="sr-only">(current)</span>
				</a></li>
				<li class="nav-item"><a class="nav-link" href="PageAddUser.jsp">Insert</a></li>
				<li class="nav-item"><a class="nav-link"
					href="CreateReport.jsp">Insert Report</a></li>
				<li class="nav-item"><a class="nav-link" href="ShowReport">Show
						Report</a></li>

			</ul>
			<%
				String sessionid = null;
					if (session.getAttribute("sessionid") == null) {
			%>
			<button type="button" class="btn btn-outline-success my-2 my-sm-0"
				data-toggle="modal" data-target="#exampleModal">Login</button>
			<%
				} else {
						sessionid = (String) session.getAttribute("sessionid");

						String userName = null;
						String sessionID = null;
						Cookie[] cookies = request.getCookies();
						if (cookies != null) {
							for (Cookie cookie : cookies) {
								if (cookie.getName().equals("sessionid"))
									userName = cookie.getValue();
								if (cookie.getName().equals("JSESSIONID"))
									sessionID = cookie.getValue();
								else {
									sessionID = session.getId();
								}
								//System.out.println("cookie.getName()" + cookie.getName());
								//System.out.println("cookie.getValue()" + cookie.getValue());
							}
						}
			%>
			<div class="dropdown">
				<button class="btn btn-secondary dropdown-toggle" type="button"
					id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
					aria-expanded="false"><%=sessionid%></button>
				<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
					<a class="dropdown-item" href="#"> Profile </a><a
						class="dropdown-item" href="Logout"> Sing Out </a>
				</div>
			</div>

			<%
				}
			%>

			<!-- Modal -->
			<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
				aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title text-muted" id="exampleModalLabel">
								Login.</h5>

						</div>
						<br>
						<div class="container">
							<form action="Login" method="post">
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text">Username</span>
									</div>
									<input type="text" class="form-control" name="username"
										placeholder="Username" required>
								</div>
								<p>
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text">Password&nbsp;</span>
									</div>
									<input type="password" class="form-control" name="password"
										placeholder="Password" required>

								</div>
								<div align="right">
									<br>
									<button type="submit" class="btn btn-outline-success">Login</button>
									<button type="button" class="btn btn-outline-danger"
										data-dismiss="modal">Close</button>
								</div>
							</form>
						</div>


					</div>
				</div>
			</div>

		</div>
		</nav>

		<div id="myCarousel" class="carousel slide" data-ride="carousel">
			<div class="item active">
				<img
					src="FileServlet?pathImage=E:\PROJECT-MYECLIPSE\WebConSql\slide\tabbar.jpg"
					class="rounded" alt="Cinque Terre" style="width:100%; height:20%"
					height="100">
				<div class="carousel-caption" align="center">
					<h3 class="font-italic">Create Report</h3>
				</div>
			</div>
		</div>
		<br>
		<!-- Message Alert!! -->
		<%
			if (resultAdd == "false") {
		%>

		<div class="alert alert-danger alert-dismissible">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>Error!</strong> There is error save file. Please try again.
		</div>
		<%
			}
		%>
		<!-- End Message Alert!! -->

		<form name="form1" method="POST" action='AddArray'
			enctype="multipart/form-data">
			<!-- enctype="multipart/form-data" -->

			<%
				if (resultAdd == "false") {
						ImgModel imgHeadModel = (ImgModel) request
								.getAttribute("imgHeadModel");

						String reportName = imgHeadModel.getReportName();
						String reportDesc = imgHeadModel.getReportDesc();
						int reportStatus = imgHeadModel.getReportStatus();

						ImgDetailModel imgDetailModel = imgHeadModel.getListDetail()
								.get(0);
						//System.out.println("Todo(147): Detail toString = " + imgDetailModel.toString());
						String titleImg = imgDetailModel.getTitleImg();
						String titleDesc = imgDetailModel.getDescImg();
						int statusimg = imgDetailModel.getStatusImg();
			%>
			<div class="row">

				<div class="col-2"></div>
				<div class="col">
					<div class="form-group row">
						<label class="col-sm-3 col-form-label">Report Name</label>
						<div class="col-9">
							<input type="text" name="reportName" class="form-control"
								placeholder="Report Name" value="<%=reportName%>" />
						</div>
					</div>

					<div class="form-group row">
						<label class="col-sm-3 col-form-label">Report Description</label>
						<div class="col-9">
							<textarea name="reportDes" class="form-control" rows="4"
								placeholder="Report Description"><%=reportDesc%></textarea>
						</div>
					</div>

					<div class="form-group row">
						<%
							if (reportStatus == 0) {
						%>
						<label class="col-sm-3 col-form-label">Status Image</label>
						<div class="col-9">
							<select class="form-control" name="reportStatus">
								<option value="1">Active</option>
								<option value="0" selected="selected">Non Active</option>
							</select>
						</div>
						<%
							} else {
						%>
						<label class="col-sm-3 col-form-label">Status Image</label>
						<div class="col-9">
							<select class="form-control" name="reportStatus">
								<option value="1" selected="selected">Active</option>
								<option value="0">Non Active</option>
							</select>
						</div>

						<%
							}
						%>
					</div>
				</div>

				<div class="col-2"></div>
			</div>
			<br>
			<!-- dynamic field -->
			<div class="row">
				<div class="col-1"></div>
				<div class="col-10">

					<div class="border">
						<div class="container border border-success">

							<br>
							<div class="row">
								<div class="col-1"></div>
								<div class="col-10">

									<div class="form-group row">
										<label class="col-sm-3 col-form-label">Title Img</label>
										<div class="col-9">
											<input id="titleImg" type="text" name="titleImg[]"
												class="form-control" placeholder="Title Img"
												value="<%=titleImg%>" />
										</div>
									</div>

									<div class="form-group row">
										<label class="col-sm-3 col-form-label">Desc Img</label>
										<div class="col-9">
											<textarea name="descImg[]" class="form-control" rows="4"
												placeholder="Description Img"><%=titleDesc%></textarea>
										</div>
									</div>

									<div class="form-group row">
										<label class="col-sm-3 col-form-label">Portrait Photo</label>
										<div class="col-9">
											<input type="file" name="photo[]" class="form-control">
										</div>
									</div>

									<div class="form-group row">
										<%
											if (statusimg == 0) {
										%>
										<label class="col-sm-3 col-form-label">Status Image</label>
										<div class="col-9">
											<select class="form-control" name="statusImg[]">
												<option value="1">Active</option>
												<option value="0" selected="selected">Non Active</option>
											</select>
										</div>
										<%
											} else {
										%>
										<label class="col-sm-3 col-form-label">Status Image</label>
										<div class="col-9">
											<select class="form-control" name="statusImg[]">
												<option value="1" selected="selected">Active</option>
												<option value="0">Non Active</option>
											</select>
										</div>

										<%
											}
										%>
									</div>
									<div class="col-1"></div>
								</div>
							</div>
						</div>
					</div>
					<p></p>
					<div class="input_fields_wrap" align="right">
						<button class="add_field_button btn btn-outline-success">+</button>
					</div>
				</div>

			</div>

			<%
				} else {
			%>
			<!-- End Message Alert!! -->
			<div class="row">

				<div class="col-2"></div>
				<div class="col">
					<br>
					<div class="form-group row">
						<label class="col-sm-3 col-form-label">Report Name</label>
						<div class="col-9">
							<input type="text" name="reportName" class="form-control"
								placeholder="Report Name" />
						</div>
					</div>

					<div class="form-group row">
						<label class="col-sm-3 col-form-label">Report Description</label>
						<div class="col-9">
							<textarea name="reportDes" class="form-control" rows="4"
								placeholder="Report Description"></textarea>
						</div>
					</div>

					<div class="form-group row">
						<label class="col-sm-3 col-form-label">Report Status</label>

						<div class="col-9">
							<select class="form-control" name="reportStatus">
								<option value="1">Active</option>
								<option value="0">Non Active</option>
							</select>
						</div>
					</div>
				</div>

				<div class="col-2"></div>
			</div>
			<br>
			<!-- dynamic field -->
			<div class="row">
				<div class="col-1"></div>
				<div class="col-10">

					<div class="border">
						<div class="container border border-success">

							<br>
							<div class="row">
								<div class="col-1"></div>
								<div class="col-10">

									<div class="form-group row">
										<label class="col-sm-3 col-form-label">Title Img</label>
										<div class="col-9">
											<input id="titleImg[]" type="text" name="titleImg[]"
												class="form-control" placeholder="Title Img" />
										</div>
									</div>

									<div class="form-group row">
										<label class="col-sm-3 col-form-label">Desc Img</label>
										<div class="col-9">
											<textarea id="descImg[]" name="descImg[]"
												class="form-control" rows="4" placeholder="Description Img"></textarea>
										</div>
									</div>

									<div class="form-group row">
										<label class="col-sm-3 col-form-label">Portrait Photo</label>
										<div class="col-9">
											<input type="file" name="photo[]" class="form-control">
										</div>
									</div>

									<div class="form-group row">
										<label class="col-sm-3 col-form-label">Status Image</label>
										<div class="col-9">
											<select class="form-control" name="statusImg[]">
												<option value="1">Active</option>
												<option value="0">Non Active</option>
											</select>
										</div>
									</div>
									<div class="col-1"></div>
								</div>
							</div>
						</div>
					</div>
					<p></p>
					<div class="input_fields_wrap" align="right">
						<button class="add_field_button btn btn-outline-success">+</button>
					</div>
				</div>

			</div>
			<%
				}
			%>

			<br>
			<div class="row">
				<div class="col"></div>
				<div class="col" align="center">
					<button type="submit" class="btn btn-outline-success">
						submit</button>
				</div>
				<div class="col" align="center">
					<button type="button" class="btn btn-outline-primary"
						onclick="fncReset();">Clear</button>
				</div>
				<div class="col" align="center">
					<button type="button" class="btn btn-outline-primary"
						onClick="history.go(-1);">Back</button>
				</div>
				<div class="col"></div>
			</div>
		</form>
		<br>
	</div>

</body>

<script type="text/javascript">
	$(document)
			.ready(
					function() {
						var max_fields = 5; //maximum input boxes allowed
						var wrapper = $(".input_fields_wrap"); //Fields wrapper
						var add_button = $(".add_field_button"); //Add button ID

						var x = 1; //initlal text box count
						$(add_button)
								.click(
										function(e) { //on add input button click
											e.preventDefault();
											if (x < max_fields) { //max input box allowed
												x++; //text box increment
												$(wrapper)
														.append(
																'<div class="form-group">'
																		+ '<br>'
																		+ '<div class="container border border-success" id="taskTemplate">'
																		+ '<br>'
																		+ '<div class="row">'
																		+ '<div class="col-1"></div>'
																		+ '<div class="col-10">'

																		+ '<div class="form-group row">'
																		+ '<label class="col-sm-3 col-form-label" align="left">Title Img</label>'
																		+ '<div class="col-9">'
																		+ '<input id="titleImg[]" type="text" name="titleImg[]" class="form-control"'+
										'placeholder="Title Img" />'
																		+ '</div>'
																		+ '</div>'

																		+ '<div class="form-group row">'
																		+ '<label class="col-sm-3 col-form-label" align="left">Desc Img</label>'
																		+ '<div class="col-9">'
																		+ '<textarea id="descImg[]" name="descImg[]" class="form-control" rows="4"'+
										'placeholder="Description Img"></textarea>'
																		+ '</div>'
																		+ '</div>'

																		+ '<div class="form-group row">'
																		+ '<label class="col-sm-3 col-form-label" align="left">Portrait Photo</label>'
																		+ '<div class="col-9">'
																		+ '<input type="file" name="photo[]" class="form-control">'
																		+ '</div>'
																		+ '</div>'

																		+ '<div class="form-group row">'
																		+ '<label class="col-sm-3 col-form-label" align="left">Status Image</label>'
																		+ '<div class="col-9" align="left">'
																		+ '<select class="form-control" name="statusImg[]">'
																		+ '<option value="1">Active</option>'
																		+ '<option value="0">Non Active</option>'
																		+ '</select>'
																		+ '</div>'
																		+ '</div>'

																		+ '</div>'

																		+ '</div>'
																		+ '<div class="col-1">'

																		+ '</div>'
																		+ '<button class="remove_field btn btn-outline-danger">-</button>'
																		+ '<p></p>'
																		+ '</div>'

																		+ '</div>'

														); //add input box
											}
										});

						$(wrapper).on("click", ".remove_field", function(e) { //user click on remove text
							e.preventDefault();
							$(this).parents('.form-group').remove();
							x--;
						})
					});
</script>
</html>
