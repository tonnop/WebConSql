package com.login.model;

public class UserModel {

	// 1. private Variable
	private String ID;
	private String Pass;
	private String Tel;
	private String Sex;
	private String Age;
	private String Address;
	private String picPath;

	// 2.use Getter&Setter
	public String getID() {
		return ID;
	}

	public void setID(String iD) {
		ID = iD;
	}

	public String getPass() {
		return Pass;
	}

	public void setPass(String pass) {
		Pass = pass;
	}

	public String getTel() {
		return Tel;
	}

	public void setTel(String tel) {
		Tel = tel;
	}

	public String getSex() {
		return Sex;
	}

	public void setSex(String sex) {
		Sex = sex;
	}

	public String getAge() {
		return Age;
	}

	public void setAge(String age) {
		Age = age;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) {
		Address = address;
	}
	
	public String getPicPath() {
		return picPath;
	}

	public void setPicPath(String picPath) {
		this.picPath = picPath;
	}

	@Override
	public String toString() {
		return "UserModel [ID=" + ID + ", Pass=" + Pass + ", Tel=" + Tel
				+ ", Sex=" + Sex + ", Age=" + Age + ", Address=" + Address
				+ ", picPath=" + picPath + "]";
	}
	
	
}
