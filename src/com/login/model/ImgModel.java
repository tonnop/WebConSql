package com.login.model;

import java.util.List;

//head
public class ImgModel {

	private int infId;
	private String reportName;
	private String reportDesc;
	private int reportStatus;
	private String date;
	private List<ImgDetailModel> listDetail;

	public int getInfId() {
		return infId;
	}

	public void setInfId(int infId) {
		this.infId = infId;
	}

	public List<ImgDetailModel> getListDetail() {
		return listDetail;
	}

	public void setListDetail(List<ImgDetailModel> listDetail) {
		this.listDetail = listDetail;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	public String getReportDesc() {
		return reportDesc;
	}

	public void setReportDesc(String reportDesc) {
		this.reportDesc = reportDesc;
	}

	public int getReportStatus() {
		return reportStatus;
	}

	public void setReportStatus(int reportStatus) {
		this.reportStatus = reportStatus;
	}

	@Override
	public String toString() {
		return "ImgModel [infId=" + infId + ", reportName=" + reportName
				+ ", reportDesc=" + reportDesc + ", reportStatus="
				+ reportStatus + ", date=" + date + ", listDetail="
				+ listDetail + "]";
	}

}
