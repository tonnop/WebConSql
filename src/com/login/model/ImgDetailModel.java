package com.login.model;

//detail
public class ImgDetailModel {
	private int refInfId;
	private String titleImg;
	private String descImg;
	private String imgPath;
	private int statusImg;
	private String oldImgPath;

	public int getRefInfId() {
		return refInfId;
	}

	public void setRefInfId(int refInfId) {
		this.refInfId = refInfId;
	}

	public String getTitleImg() {
		return titleImg;
	}

	public void setTitleImg(String titleImg) {
		this.titleImg = titleImg;
	}

	public String getDescImg() {
		return descImg;
	}

	public void setDescImg(String descImg) {
		this.descImg = descImg;
	}

	public String getImgPath() {
		return imgPath;
	}

	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}

	public int getStatusImg() {
		return statusImg;
	}

	public void setStatusImg(int statusImg) {
		this.statusImg = statusImg;
	}

	public String getOldImgPath() {
		return oldImgPath;
	}

	public void setOldImgPath(String oldImgPath) {
		this.oldImgPath = oldImgPath;
	}

	@Override
	public String toString() {
		return "ImgDetailModel [refInfId=" + refInfId + ", titleImg="
				+ titleImg + ", descImg=" + descImg + ", imgPath=" + imgPath
				+ ", statusImg=" + statusImg + ", oldImgPath=" + oldImgPath
				+ "]";
	}

}
