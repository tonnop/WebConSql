package com.login.action;

import java.io.*;

import javax.servlet.*;
import javax.servlet.http.*;

import com.terk.logic.UserLogic;

public class DeleteUserAction extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doAction(req, resp);
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doAction(req, resp);
	}// end doget

	private void doAction(HttpServletRequest req, HttpServletResponse resp) {

		String id = req.getParameter("id");
		String photo = req.getParameter("photo");

		try {

			UserLogic s = new UserLogic();
			boolean result = s.Userdelete(id);

			if (result == true) {
				File filePath = new File(photo);
				if (filePath.delete()) {
					System.out.println(filePath.getName() + " is deleted!");
				} else {
					System.out.println("Delete operation is failed.");
				}
				req.setAttribute("result", result);
				req.getRequestDispatcher("ShowUser").forward(req, resp);
			}
			else if (result == false){
				req.setAttribute("result", result);
				req.getRequestDispatcher("ShowUser").forward(req, resp);
			}


		}// end try
		catch (Exception e) {
			e.printStackTrace();
		}// end catch

	}// end doaction

}// end class

// https://stackoverflow.com/questions/13685592/java-cannot-delete-file-on-windows