package com.login.action;

import java.io.*;

import javax.servlet.*;
import javax.servlet.http.*;

import com.terk.logic.UserLogic;

public class LoginServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doAction(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doAction(req, resp);
	}

	protected void doAction(HttpServletRequest req, HttpServletResponse resp) {

		String id = req.getParameter("username");
		String pass = req.getParameter("password");
		

		try {
			UserLogic logic = new UserLogic();
			boolean result = logic.LoginLogic(id, pass);
			System.out.println("Login result = " + result);

			if (result) {
				HttpSession session = req.getSession();
				session.setAttribute("sessionid", id);
				session.setMaxInactiveInterval(30*60);
				
				Cookie username = new Cookie("sessionid",id);
				resp.addCookie(username);

				resp.sendRedirect("ShowUser");
			}// End if
			else {
				
				resp.sendRedirect("ShowUser");

			}// End else

		}// End Try
		catch (Exception e) {
			e.printStackTrace();
		}// End Catch

	}

}
