package com.login.action;

import java.io.*;
import java.util.*;
import java.util.regex.Pattern;

import javax.servlet.*;
import javax.servlet.http.*;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.login.model.ImgDetailModel;
import com.login.model.ImgModel;
import com.login.model.UserModel;
import com.terk.logic.UserLogic;

public class EditReportAction extends HttpServlet {

	private String UPLOAD_DIRECTORY = "E:\\PROJECT-MYECLIPSE\\WebConSql\\upload";
	private int THRESHOLD_SIZE = 1024 * 1024 * 3; // 3MB
	private int MAX_FILE_SIZE = 1024 * 1024 * 40; // 40MB
	private int MAX_REQUEST_SIZE = 1024 * 1024 * 50; // 50MB

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doAction(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doAction(req, resp);
	}

	private void doAction(HttpServletRequest req, HttpServletResponse resp) {

		DateFormatAction dfa = new DateFormatAction();
		String createDateFormat = dfa.createdateFormat();
		DiskFileItemFactory factory = new DiskFileItemFactory();
		factory.setSizeThreshold(THRESHOLD_SIZE);
		factory.setRepository(new File(System.getProperty("java.io.tmpdir")));
		ServletFileUpload upload = new ServletFileUpload(factory);
		upload.setFileSizeMax(MAX_FILE_SIZE);
		upload.setSizeMax(MAX_REQUEST_SIZE);

		ImgModel imgModel = new ImgModel();
		String oldphoto_path = "";
		String infid = req.getParameter("infid");

		if (req.getParameter("infid") != null) {


			try {
				UserLogic s = new UserLogic();
				s.ReportEditSearch(infid, imgModel);
				System.out.println(imgModel.toString());
				req.setAttribute("imgModel", imgModel);
				req.getRequestDispatcher("EditReport.jsp").forward(req, resp);

			}// end try
			catch (Exception e) {
				// Auto-generated catch block
				e.printStackTrace();
			}// end catch

		}// end if
		else { // Parameter null because method : post & multipart/form-data

			File uploadDir = new File(UPLOAD_DIRECTORY);
			if (!uploadDir.exists()) {
				uploadDir.mkdir();
			}
			try {
				// parses the request's content to extract file data
				List formItems = upload.parseRequest(req);
				Iterator iter = formItems.iterator();

				ImgDetailModel detailModel = new ImgDetailModel();
				List<ImgDetailModel> listDetail = new ArrayList<ImgDetailModel>();

				// iterates over form's fields
				while (iter.hasNext()) {
					FileItem item = (FileItem) iter.next();

					// processes only fields that are not form fields
					if (!item.isFormField()) {// chk file upload

						if (item.getName() != "" && item.getName() != null) {
							String[] fileExten = item.getName().split(
									Pattern.quote("."));

							if (fileExten[1].equals("jpg")
									|| fileExten[1].equals("jpeg")
									|| fileExten[1].equals("png")
									|| fileExten[1].equals("gif")) {

								String dateFormat = dfa.dateFormat();
								String reName = detailModel.getTitleImg() + "_"
										+ dateFormat + "." + fileExten[1];

								String filePath = UPLOAD_DIRECTORY
										+ File.separator + reName;
								File storeFile = new File(filePath);
								// saves the file on disk
								item.write(storeFile);

								detailModel.setImgPath(UPLOAD_DIRECTORY
										+ File.separator + reName);

								listDetail.add(detailModel);
								imgModel.setListDetail(listDetail);

							}// End If Check file image
						}// End If Check Name Null
						else {
							detailModel.setImgPath("");
							listDetail.add(detailModel);
							imgModel.setListDetail(listDetail);

						}
					} // here coding
					else { // chk file upload

						String val = item.getString();
						String fieldName = item.getFieldName(); // search field
																// name in jsp
																// page
						if ("infid".equals(fieldName)) {
							infid = val;
						} else if ("reportName".equals(fieldName)) {
							imgModel.setReportName(val);
						} else if ("reportDes".equals(fieldName)) {
							imgModel.setReportDesc(val);
						} else if ("reportStatus".equals(fieldName)) {
							imgModel.setReportStatus(Integer.parseInt(val));
						} else if ("txnId[]".equals(fieldName)) {
							detailModel.setRefInfId(Integer.parseInt(val));
						} else if ("titleImg[]".equals(fieldName)) {
							detailModel.setTitleImg(val);
						} else if ("descImg[]".equals(fieldName)) {
							detailModel.setDescImg(val);
						} else if ("statusImg[]".equals(fieldName)) {
							detailModel.setStatusImg(Integer.parseInt(val));
							imgModel.setDate(createDateFormat);
						} else if ("oldphoto".equals(fieldName)) {
							detailModel.setOldImgPath(val);
							System.out.println("Todo(151): OldImgPath = "
									+ detailModel.getOldImgPath());
							detailModel = new ImgDetailModel();
						}
					}// end else

				}// end while

				UserLogic s = new UserLogic();
				boolean resultEdit = s.ReportEdit(infid, imgModel);

				System.out.println("Todo(162): resultEdit = " + resultEdit);

				if (resultEdit == true) {
					System.out.println("Todo(162): Update successfully");

					for (int i = 0; i < imgModel.getListDetail().size(); i++) {
						ImgDetailModel imgDetailModel = imgModel
								.getListDetail().get(i);
						if (!imgDetailModel.getImgPath().equals("")) {
							if (!imgDetailModel.getOldImgPath().equals("")) {
								File filePathDel = new File(
										imgDetailModel.getOldImgPath());
								if (filePathDel.delete()) {
									System.out.println("Todo(154) : "
											+ filePathDel.getName()
											+ " is deleted!");
								}
							} else {
								System.out
										.println("Todo(178): OldImgPath is null");
							}// end else
						} else {
							System.out.println("Todo(181): ImgPath is null");
						}// end else
					}// end for
					req.setAttribute("resultEdit", resultEdit);
					req.getRequestDispatcher("ShowReport").forward(req, resp);
				}// end if result true
				else if (resultEdit == false) {
					System.out.println("Todo(186): Update not successfully");

					// if Update fail delete img in server
					for (int i = 0; i < imgModel.getListDetail().size(); i++) {
						ImgDetailModel imgDetailModel = imgModel
								.getListDetail().get(i);
						if (!imgDetailModel.getImgPath().equals("")) {

							File filePathDel = new File(
									imgDetailModel.getImgPath());
							if (filePathDel.delete()) {
								System.out.println("Todo(196) : "
										+ filePathDel.getName()
										+ " is deleted!");
							}
						} else {
							System.out.println("Todo(202): ImgPath is null");
						}
					}
					req.setAttribute("resultEdit", resultEdit);
					req.setAttribute("imgModel", imgModel);
					req.getRequestDispatcher("EditReport.jsp").forward(req,
							resp);
				}

			}// end try
			catch (Exception e) {
				// Auto-generated catch block
				e.printStackTrace();
			}// end catch

		}// end else

	}
}
