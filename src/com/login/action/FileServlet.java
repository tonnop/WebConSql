package com.login.action;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class FileServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doAction(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doAction(req, resp);
	}

	private void doAction(HttpServletRequest req, HttpServletResponse resp) {

		try {

			String fileName = req.getParameter("pathImage");
			FileInputStream fis = new FileInputStream(new File(fileName));
			BufferedInputStream bis = new BufferedInputStream(fis);
			// resp.setContentType(contentType);
			BufferedOutputStream output = new BufferedOutputStream(
					resp.getOutputStream());
			for (int data; (data = bis.read()) > -1;) {
				output.write(data);
			}

			fis.close();
			bis.close();
			output.close();
		} catch (IOException e) {

		} finally {
			// close the streams
		}

	}

}
