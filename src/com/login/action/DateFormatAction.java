package com.login.action;

import java.text.SimpleDateFormat;
import java.util.*;

public class DateFormatAction {

	public String dateFormat() {

		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss");
		Date fromdate = new Date();
		String date = formatter.format(fromdate);

		return date;
	}
	
	public String createdateFormat() {

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date fromdate = new Date();
		String date = formatter.format(fromdate);

		return date;
	}
}
