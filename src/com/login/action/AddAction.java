package com.login.action;

import java.io.*;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

import javax.servlet.*;
import javax.servlet.http.*;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.login.model.UserModel;
import com.terk.logic.UserLogic;

public class AddAction extends HttpServlet {

	private final String UPLOAD_DIRECTORY = "E:\\PROJECT-MYECLIPSE\\WebConSql\\upload";
	private final int THRESHOLD_SIZE = 1024 * 1024 * 3; // 3MB
	private final int MAX_FILE_SIZE = 1024 * 1024 * 40; // 40MB
	private final int MAX_REQUEST_SIZE = 1024 * 1024 * 50; // 50MB

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doAction(req, resp);
	}// end doget

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doAction(req, resp);// TODOO
	}

	public void doAction(HttpServletRequest req, HttpServletResponse resp) {

		DiskFileItemFactory factory = new DiskFileItemFactory();
		factory.setSizeThreshold(THRESHOLD_SIZE);
		factory.setRepository(new File(System.getProperty("java.io.tmpdir")));
		ServletFileUpload upload = new ServletFileUpload(factory);
		upload.setFileSizeMax(MAX_FILE_SIZE);
		upload.setSizeMax(MAX_REQUEST_SIZE);

		File uploadDir = new File(UPLOAD_DIRECTORY);
		if (!uploadDir.exists()) {
			uploadDir.mkdir();
		}
		try {
			// parses the request's content to extract file data
			List formItems = upload.parseRequest(req);
			Iterator iter = formItems.iterator();

			UserModel userModel = new UserModel();

			// iterates over form's fields
			while (iter.hasNext()) {
				FileItem item = (FileItem) iter.next();

				if (!item.isFormField()) {// chk file upload
					if (item.getName() != "" && item.getName() != null) {
						String[] fileExten = item.getName().split(
								Pattern.quote("."));

						if (fileExten[1].equals("jpg")
								|| fileExten[1].equals("jpeg")
								|| fileExten[1].equals("png")
								|| fileExten[1].equals("gif")) {

							DateFormatAction dfa = new DateFormatAction();
							String dateFormat = dfa.dateFormat();
							String reName = userModel.getID() + "_"
									+ dateFormat + "." + fileExten[1];

							System.out.println("item.getname = "
									+ item.getName());

							String filePath = UPLOAD_DIRECTORY + File.separator
									+ reName;
							File storeFile = new File(filePath);

							// saves the file on disk
							item.write(storeFile);
							// System.out.println("filePath = " + filePath);
							userModel.setPicPath(UPLOAD_DIRECTORY
									+ File.separator + reName);

						}// End If Check file image
					}// End If Check Name Null
					else {
						userModel.setPicPath("");
					}
				}// here coding
				else {
					String val = item.getString();
					String fieldName = item.getFieldName(); // search field name
															// in jsp page
					if ("id".equals(fieldName)) {
						userModel.setID(val);
					} else if ("pass".equals(fieldName)) {
						userModel.setPass(val);
					} else if ("tel".equals(fieldName)) {
						userModel.setTel(val);
					} else if ("sex".equals(fieldName)) {
						userModel.setSex(val);
					} else if ("age".equals(fieldName)) {
						userModel.setAge(val);
					} else if ("address".equals(fieldName)) {
						userModel.setAddress(val);
					}
				}// end else

			}// end while
			UserLogic s = new UserLogic();
			boolean resultAdd = s.Useradd(userModel);
			
			if (resultAdd == true) {
				req.setAttribute("resultAdd", resultAdd);
				req.getRequestDispatcher("ShowUser").forward(req, resp);
			} else {
				File filePathDel = new File(userModel.getPicPath());
				if (filePathDel.delete()) {
					System.out.println("Todo(196) : " + filePathDel.getName()
							+ " is deleted!");
				}
				req.setAttribute("resultAdd", resultAdd);
				req.setAttribute("userModel", userModel);
				req.getRequestDispatcher("PageAddUser.jsp").forward(req, resp);
			}

		}// end try
		catch (Exception e) {
			e.printStackTrace();
		}// end catch

	}

}// end class
