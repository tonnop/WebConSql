package com.login.action;

import java.io.*;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

import javax.servlet.*;
import javax.servlet.http.*;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.login.model.ImgDetailModel;
import com.login.model.UserModel;
import com.terk.logic.UserLogic;

public class EditAction extends HttpServlet {

	private String UPLOAD_DIRECTORY = "E:\\PROJECT-MYECLIPSE\\WebConSql\\upload";
	private int THRESHOLD_SIZE = 1024 * 1024 * 3; // 3MB
	private int MAX_FILE_SIZE = 1024 * 1024 * 40; // 40MB
	private int MAX_REQUEST_SIZE = 1024 * 1024 * 50; // 50MB

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doAction(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doAction(req, resp);
	}

	private void doAction(HttpServletRequest req, HttpServletResponse resp) {

		DiskFileItemFactory factory = new DiskFileItemFactory();
		factory.setSizeThreshold(THRESHOLD_SIZE);
		factory.setRepository(new File(System.getProperty("java.io.tmpdir")));
		ServletFileUpload upload = new ServletFileUpload(factory);
		upload.setFileSizeMax(MAX_FILE_SIZE);
		upload.setSizeMax(MAX_REQUEST_SIZE);
		String oldphoto_path = "";
		String id = "";

		System.out.println("doAction*");

		if (req.getParameter("id") != null) {

			UserModel userModel = new UserModel();
			id = req.getParameter("id");

			try {
				UserLogic s = new UserLogic();
				s.Useredit(userModel, id);

				req.setAttribute("userModel", userModel);
				req.getRequestDispatcher("UpdateUser.jsp").forward(req, resp);

			}// end try
			catch (Exception e) {
				// Auto-generated catch block
				e.printStackTrace();
			}// end catch

		}// end if

		else { // Parameter null because method : post & multipart/form-data

			File uploadDir = new File(UPLOAD_DIRECTORY);
			if (!uploadDir.exists()) {
				uploadDir.mkdir();
			}
			try {
				// parses the request's content to extract file data
				List formItems = upload.parseRequest(req);
				Iterator iter = formItems.iterator();

				UserModel userModel = new UserModel();

				// iterates over form's fields
				while (iter.hasNext()) {
					FileItem item = (FileItem) iter.next();

					System.out.println(" - item.getFieldName : "
							+ item.getFieldName());
					// processes only fields that are not form fields
					if (item.isFormField()) {// chk file upload

						String val = item.getString();
						String fieldName = item.getFieldName(); // search field
																// name in jsp
																// page
						if ("id".equals(fieldName)) {
							id = val;
						} else if ("id2".equals(fieldName)) {
							userModel.setID(val);
						} else if ("pass2".equals(fieldName)) {
							userModel.setPass(val);
						} else if ("tel2".equals(fieldName)) {
							userModel.setTel(val);
						} else if ("sex2".equals(fieldName)) {
							userModel.setSex(val);
						} else if ("age2".equals(fieldName)) {
							userModel.setAge(val);
						} else if ("address2".equals(fieldName)) {
							userModel.setAddress(val);
						} else if ("oldphoto".equals(fieldName)) {
							oldphoto_path = val;
						}

					}// end if !item.isForm
					if (!item.isFormField()) {
						if (item.getName() != "") {

							String[] teststr = item.getName().split(
									Pattern.quote("."));

							DateFormatAction dfa = new DateFormatAction();
							String dateFormat = dfa.dateFormat();
							String reName = userModel.getID() + "_"
									+ dateFormat + "." + teststr[1];

							// fileName = new File(item.getName()).getName();
							String filePath = UPLOAD_DIRECTORY + File.separator
									+ reName;
							File storeFile = new File(filePath);
							// saves the file on disk
							item.write(storeFile);
							userModel.setPicPath(UPLOAD_DIRECTORY
									+ File.separator + reName);
						}// end if item.getname
						else {
							userModel.setPicPath("");
						}

					}// end if

				}// end while

				System.out.println("(94) userModel : " + userModel.toString());
				UserLogic s = new UserLogic();
				boolean resultEdit = s.Useredit(userModel, id);

				if (resultEdit == true) {

					if (!userModel.getPicPath().equals("")) {
						if (!oldphoto_path.equals("")) {
							File filePathDel = new File(oldphoto_path);
							if (filePathDel.delete()) {
								System.out.println("Todo(154) : "
										+ filePathDel.getName()
										+ " is deleted!");
							}
						} else {
							System.out.println("Todo(178): OldImgPath is null");
						}// end else
					} else {
						System.out.println("Todo(181): ImgPath is null");
					}// end else

					req.setAttribute("resultEdit", resultEdit);
					req.getRequestDispatcher("ShowUser").forward(req, resp);
				} else if (resultEdit == false) {

					if (!userModel.getPicPath().equals("")) {

						File filePathDel = new File(userModel.getPicPath());
						if (filePathDel.delete()) {
							System.out.println("Todo(196) : "
									+ filePathDel.getName() + " is deleted!");
						}
					} else {
						System.out.println("Todo(202): ImgPath is null");
					}

					req.setAttribute("userModel", userModel);
					req.setAttribute("resultEdit", resultEdit);
					req.getRequestDispatcher("UpdateUser.jsp").forward(req,
							resp);
				}

			}// end try
			catch (Exception e) {
				// Auto-generated catch block
				e.printStackTrace();
			}// end catch

		}// end else

	}// end doaction

}// end class
