package com.login.action;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

import com.login.model.UserModel;
import com.terk.logic.UserLogic;

public class ProfileAction extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doAction(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doAction(req, resp);
	}

	protected void doAction(HttpServletRequest req, HttpServletResponse resp) {

		try {
			UserModel userModel = new UserModel();

			resp.setContentType("text/html");
			Cookie[] cookies = req.getCookies();
			HttpSession session = req.getSession(false);// don't create if
															// it doesn't exist
			if (session != null && !session.isNew()) {

				if (cookies != null) {
					for (Cookie cookie : cookies) {
						if (cookie.getName().equals("sessionid")) {
							userModel.setID(cookie.getValue());
							System.out
									.println("sessionid=" + userModel.getID());
						}
						if (cookie.getName().equals("JSESSIONID")) {
							System.out.println("JSESSIONID="
									+ cookie.getValue());
						}
					}

					UserLogic logic = new UserLogic();
					boolean result = logic.ProfileSearch(userModel);

					if (result) {
						req.setAttribute("userModel", userModel);
						req.getRequestDispatcher("MyProfile.jsp").forward(req,
								resp);
					} else {
						req.setAttribute("resultProfile", result);
						req.getRequestDispatcher("ShowUser").forward(req, resp);
					}

				}
			} else {
				resp.sendRedirect("ShowUser");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
