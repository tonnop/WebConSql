package com.login.action;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

import com.login.model.ImgDetailModel;
import com.terk.logic.UserLogic;

public class DeleteReportAction extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doAction(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doAction(req, resp);
	}

	private void doAction(HttpServletRequest req, HttpServletResponse resp) {

		List<ImgDetailModel> listDetail = new ArrayList<ImgDetailModel>();
		String infid = req.getParameter("infid");
		String txnId = req.getParameter("txnId");
		String imgPath = "";

		try {
			
			if(txnId != null){
				
				UserLogic s = new UserLogic();
				boolean resultDetailDelete = s.ReportDetailDelete(txnId, imgPath);
				
				if(resultDetailDelete){
					if (imgPath != ""){
					File filePath = new File(imgPath);
					filePath.delete();
					}
					req.setAttribute("resultDetailDelete", resultDetailDelete);
					req.setAttribute("infid", infid);
					req.getRequestDispatcher("EditReport").forward(req, resp);
				}
				else if(!resultDetailDelete){
					req.setAttribute("resultDetailDelete", resultDetailDelete);
					req.setAttribute("infid", infid);
					req.getRequestDispatcher("EditReport").forward(req, resp);
				}
				
			}//End delete detail
			else{

			UserLogic s = new UserLogic();
			boolean resultDelete = s.Reportdelete(infid, listDetail);
			System.out.println("Todo(34) :result = " + resultDelete);
			int i = 0;
			if (resultDelete == true) {

				if (listDetail.size() > 0) {

					while (i < listDetail.size()) {
						ImgDetailModel imgDetailModel = listDetail.get(i);
						String imgPaht = imgDetailModel.getImgPath();
						i++;

						System.out.println("Todo(45) :imgPath = " + imgPaht);
						File filePath = new File(imgPaht);
						if (filePath.delete()) {
							System.out.println(filePath.getName()
									+ " is deleted!");
						} else {
							System.out.println("Delete operation is failed.");
						}

					}// End while
				}// End if check size
				req.setAttribute("resultDelete", resultDelete);
				req.getRequestDispatcher("ShowReport").forward(req, resp);
			}// End if result true
			else if (resultDelete == false) {
				req.setAttribute("resultDelete", resultDelete);
				req.getRequestDispatcher("ShowReport").forward(req, resp);
			}// End else if result false
			}//End else delete detail


		}// end try
		catch (Exception e) {
			e.printStackTrace();
		}// end catch

	}

}
// https://stackoverflow.com/questions/13685592/java-cannot-delete-file-on-windows