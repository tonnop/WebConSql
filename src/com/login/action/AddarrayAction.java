package com.login.action;

import java.io.*;
import java.text.Normalizer.Form;
import java.util.*;

import java.util.regex.Pattern;
import javax.servlet.*;

import javax.servlet.http.*;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.login.model.ImgDetailModel;
import com.login.model.ImgModel;
import com.terk.logic.UserLogic;

public class AddarrayAction extends HttpServlet {

	private final String UPLOAD_DIRECTORY = "E:\\PROJECT-MYECLIPSE\\WebConSql\\upload";
	private final int THRESHOLD_SIZE = 1024 * 1024 * 3; // 3MB
	private final int MAX_FILE_SIZE = 1024 * 1024 * 40; // 40MB
	private final int MAX_REQUEST_SIZE = 1024 * 1024 * 50; // 50MB

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doAction(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doAction(req, resp);
	}

	private void doAction(HttpServletRequest req, HttpServletResponse resp) {

		DateFormatAction dfa = new DateFormatAction();
		String createDateFormat = dfa.createdateFormat();
		DiskFileItemFactory factory = new DiskFileItemFactory();
		factory.setSizeThreshold(THRESHOLD_SIZE);
		factory.setRepository(new File(System.getProperty("java.io.tmpdir")));
		ServletFileUpload upload = new ServletFileUpload(factory);
		upload.setFileSizeMax(MAX_FILE_SIZE);
		upload.setSizeMax(MAX_REQUEST_SIZE);

		File uploadDir = new File(UPLOAD_DIRECTORY);
		if (!uploadDir.exists()) {
			uploadDir.mkdir();
		}
		try {
			// parses the request's content to extract file data
			List formItems = upload.parseRequest(req);
			Iterator iter = formItems.iterator();

			ImgModel imgHeadModel = new ImgModel();
			ImgDetailModel detailModel = new ImgDetailModel();
			List<ImgDetailModel> detailModelAr = new ArrayList<ImgDetailModel>();

			// iterates over form's fields
			while (iter.hasNext()) {
				FileItem item = (FileItem) iter.next();

				if (!item.isFormField()) {// chk file upload
					if (item.getName() != "" && item.getName() != null) {
						String[] fileExten = item.getName().split(
								Pattern.quote("."));

						if (fileExten[1].equals("jpg")
								|| fileExten[1].equals("jpeg")
								|| fileExten[1].equals("png")
								|| fileExten[1].equals("gif")) {

							String dateFormat = dfa.dateFormat();
							String reName = detailModel.getTitleImg() + "_"
									+ dateFormat + "." + fileExten[1];

							System.out.println("item.getname = "
									+ item.getName());

							String filePath = UPLOAD_DIRECTORY + File.separator
									+ reName;
							System.out.println("filePath = " + filePath);
							File storeFile = new File(filePath);

							// saves the file on disk
							item.write(storeFile);
							detailModel.setImgPath(UPLOAD_DIRECTORY
									+ File.separator + reName);

						}// End If Check file image
					}// End If Check Name Null
					else {
						detailModel.setImgPath("");
					}
				} // here coding
				else {
					String val = item.getString();
					String fieldName = item.getFieldName(); // search field name
					// in jsp page
					
					if ("reportName".equals(fieldName)) {
						imgHeadModel.setReportName(val);
					} else if ("reportDes".equals(fieldName)) {
						imgHeadModel.setReportDesc(val);
						System.out.println(imgHeadModel.getReportName());
					} else if ("reportStatus".equals(fieldName)) {
						imgHeadModel.setReportStatus(Integer.parseInt(val));
					} else if ("titleImg[]".equals(fieldName)) {
						detailModel.setTitleImg(val);
						System.out.println(detailModel.getTitleImg());
					} else if ("descImg[]".equals(fieldName)) {
						detailModel.setDescImg(val);
					} else if ("statusImg[]".equals(fieldName)) {
						detailModel.setStatusImg(Integer.parseInt(val));
						imgHeadModel.setDate(createDateFormat);
						detailModelAr.add(detailModel);
						
						detailModel = new ImgDetailModel();
					}
				}// end else

			}// end while
			imgHeadModel.setListDetail(detailModelAr);

			UserLogic s = new UserLogic();
			boolean resultAdd = s.ReportAdd(imgHeadModel);

			if (resultAdd) {
				System.out.println("Todo(132) : Insert successfully");
				req.setAttribute("resultAdd", resultAdd);
				req.getRequestDispatcher("ShowReport").forward(req, resp);
			} else if (!resultAdd) {
				System.out.println("Todo(136) : Insert not successfully");

				// if Update fail delete img in server
				for (int i = 0; i < imgHeadModel.getListDetail().size(); i++) {
					ImgDetailModel imgDetailModel = imgHeadModel
							.getListDetail().get(i);
					if (!imgDetailModel.getImgPath().equals("")) {

						File filePathDel = new File(imgDetailModel.getImgPath());
						if (filePathDel.delete()) {
							System.out.println("Todo(146) : "
									+ filePathDel.getName() + " is deleted!");
						}
					} else {
						System.out.println("Todo(150): ImgPath is null");
					}
				}

				req.setAttribute("resultAdd", resultAdd);
				req.setAttribute("imgHeadModel", imgHeadModel);
				req.getRequestDispatcher("CreateReport.jsp").forward(req, resp);
			}
		}// end try

		catch (Exception e) {
			e.printStackTrace();
		}// end catch

	}
}
// //-----------------------example
// List<ImgDetailModel> listDetail = new ArrayList<ImgDetailModel>();
// //for
// ImgDetailModel detailModelExample = new ImgDetailModel();
// detailModelExample.setDescImg("");
// detailModelExample.setStatusImg("");
//
// listDetail.add(detailModelExample);
// //end for
// imgHeadModel.setListDetail(listDetail);
// //-----------------------example
// //Boolean result = s.ReportAdd(imgHeadModel);
