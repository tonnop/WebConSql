package com.login.action;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class LogoutServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doAction(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doAction(req, resp);
	}

	protected void doAction(HttpServletRequest req, HttpServletResponse resp) {

		try {
			resp.setContentType("text/html");
			Cookie[] cookies = req.getCookies();
			if (cookies != null) {
				for (Cookie cookie : cookies) {
					if (cookie.getName().equals("JSESSIONID")) {
						System.out.println("JSESSIONID=" + cookie.getValue());
					}
		    		cookie.setMaxAge(0);
		    		resp.addCookie(cookie);
				}
			}
			//invalidate the session if exists
			HttpSession session = req.getSession(false);
			System.out.println("User=" + session.getAttribute("user"));
			if (session != null) {
				session.invalidate();
			}
			resp.sendRedirect("ShowUser");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
