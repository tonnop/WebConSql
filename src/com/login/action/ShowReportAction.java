package com.login.action;

import java.io.*;
import java.util.*;

import javax.servlet.*;
import javax.servlet.http.*;

import com.login.model.ImgModel;
import com.login.model.UserModel;
import com.terk.logic.UserLogic;

public class ShowReportAction extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doaction(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doaction(req, resp);
	}
	
	private void doaction (HttpServletRequest req, HttpServletResponse resp){
		
		try {
			UserLogic s = new UserLogic();
			List<ImgModel> listModel = s.ReportSearch();
			System.out.println(listModel.toString());
			
			
			req.setAttribute("listModel", listModel);
			req.getRequestDispatcher("ShowReport.jsp").forward(req, resp);

		}// end try
		catch (Exception e) {
			// Auto-generated catch block
			System.out.println(e.getMessage());
			e.printStackTrace();
		}// end catch
		
		
	}

}
