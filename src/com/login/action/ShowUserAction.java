package com.login.action;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;

import com.login.model.UserModel;
import com.terk.logic.UserLogic;

public class ShowUserAction extends HttpServlet {
	// create list model 1
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doAction(req, resp);
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doAction(req, resp);
	}

	private void doAction(HttpServletRequest req, HttpServletResponse resp) {

		UserModel se = new UserModel();
		se.setID(req.getParameter("sid"));
		se.setPass(req.getParameter("spass"));
		se.setTel(req.getParameter("stel"));
		se.setSex(req.getParameter("ssex"));
		se.setAge(req.getParameter("sage"));
		se.setAddress(req.getParameter("saddress"));

		try {
			UserLogic s = new UserLogic();
			List<UserModel> ListUser = s.Usersearch(null, se);

			req.setAttribute("se", se);
			req.setAttribute("ListUser", ListUser);
			req.getRequestDispatcher("ReportUser.jsp").forward(req, resp);

		}// end try
		catch (Exception e) {
			// Auto-generated catch block
			System.out.println(e.getMessage());
			e.printStackTrace();
		}// end catch

	}

}
