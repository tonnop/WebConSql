package com.terk.manage;

import java.sql.*;

public class ConDB {

	private String driver = "com.mysql.jdbc.Driver";
	private String urlcon = "jdbc:mysql://localhost:3306/mylogin?useUnicode=true&characterEncoding=UTF-8";
	private String usercon = "root";
	private String passcon = "0000";
	private Connection connect = null;

	public Connection ConDB() {
		try {
			Class.forName(driver);
			connect = DriverManager.getConnection(urlcon, usercon, passcon);
		}// end try
		catch (Exception e) {
			e.printStackTrace();
		}// end catch

		return connect;
	}// end ConDB()
}// end class ConDB