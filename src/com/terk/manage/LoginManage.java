package com.terk.manage;

import java.io.*;
import java.util.*;
import java.sql.*;

import com.login.model.UserModel;

public class LoginManage {

	public boolean LoginUser(String id, String pass) {

		ConDB db = new ConDB();
		PreparedStatement ps = null;
		String sql = "";
		boolean result = false;
		Connection con = null;

		try {
			con = db.ConDB();
			sql = "SELECT * FROM tbl_md_user where id=? and pass=?";
			ps = con.prepareStatement(sql);
			ps.setString(1, id);
			ps.setString(2, pass);
			ResultSet rs = ps.executeQuery();
			result = rs.next();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;

	}

	public boolean ProflieSearchManage(UserModel userModel) {

		ConDB db = new ConDB();
		PreparedStatement ps = null;
		String sql = "";
		boolean result = false;

		try {

			sql = "SELECT * FROM tbl_md_user WHERE id = ?";
			ps = db.ConDB().prepareStatement(sql);
			ps.setString(1, userModel.getID());

			ResultSet rec = ps.executeQuery();
			while ((rec.next()) && (rec != null)) {
				userModel.setID(rec.getString("id"));
				userModel.setPass(rec.getString("pass"));
				userModel.setTel(rec.getString("tel"));
				userModel.setSex(rec.getString("sex"));
				userModel.setAge(rec.getString("age"));
				userModel.setAddress(rec.getString("address"));
				userModel.setPicPath(rec.getString("picture_path"));
			}
			result = true;

		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

}
