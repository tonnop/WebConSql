package com.terk.manage;

import java.sql.*;
import java.util.*;

import com.login.model.ImgDetailModel;
import com.login.model.ImgModel;
import com.login.model.UserModel;
import com.terk.manage.ConDB;

public class UserManager {

	public boolean ManagetAdd(UserModel userModel) {
		ConDB db = new ConDB();
		PreparedStatement ps = null;
		String sql = "";
		boolean result = false;
		Connection con = null;

		try {
			con = db.ConDB();
			con.setAutoCommit(false);
			
			sql = "INSERT INTO tbl_md_user (id,pass,tel,sex,age,address,picture_path) VALUE (?,?,?,?,?,?,?)";
			ps = con.prepareStatement(sql);

			ps.setString(1, userModel.getID());
			ps.setString(2, userModel.getPass());
			ps.setString(3, userModel.getTel());
			ps.setString(4, userModel.getSex());
			ps.setString(5, userModel.getAge());
			ps.setString(6, userModel.getAddress());
			ps.setString(7, userModel.getPicPath());
			ps.executeUpdate();
			
			con.commit();
			result = true;
			System.out.println("insert successfully");
			
		}// end try
		catch (Exception se) {
			// Handle errors for JDBC
			se.printStackTrace();
			System.out.println("not successfully");
			// If there is an error then rollback the changes.
			System.out.println("Todo(47) : Rolling back data here....");
			try {
				if (con != null) {
					con.rollback();
					result = false;
				}// end try
				System.out.println("JDBC Transaction rolled back successfully");
			} catch (SQLException se2) {
				System.out.println("Todo(55) : SQLException in rollback = "
						+ se.getMessage());

			}// end catch
		}// end catch
		try {
			if (ps != null) {
				ps.close();
				db.ConDB().close();
			}// end if
		}// end try
		catch (SQLException e) {
			e.printStackTrace();
		}// end catch
		return result;
	}// end method ManagetAdd

	public List<UserModel> ManagetSearch(UserModel userModel, UserModel se) {
		ConDB db = new ConDB();
		PreparedStatement ps = null;
		String sql = "";
		List<UserModel> ListUser = new ArrayList<UserModel>();

		try {

			if (se.getID() != null || se.getPass() != null
					|| se.getTel() != null || se.getSex() != null
					|| se.getAge() != null || se.getAddress() != null) {
				sql = "SELECT * FROM tbl_md_user WHERE id LIKE ? AND pass LIKE ?"
						+ " AND tel LIKE ? AND sex LIKE ? AND age LIKE ?"
						+ " AND address LIKE ? ";

				ps = db.ConDB().prepareStatement(sql);
				ps.setString(1, "%" + se.getID() + "%");
				ps.setString(2, "%" + se.getPass() + "%");
				ps.setString(3, "%" + se.getTel() + "%");
				ps.setString(4, "%" + se.getSex() + "%");
				ps.setString(5, "%" + se.getAge() + "%");
				ps.setString(6, "%" + se.getAddress() + "%");
			}// end if
			else {
				sql = "SELECT * FROM  tbl_md_user ORDER BY id";
				ps = db.ConDB().prepareStatement(sql);
			}// end else

			ResultSet rec = ps.executeQuery();
			while ((rec != null) && (rec.next())) {
				UserModel U = new UserModel();
				U.setID(rec.getString("id"));
				U.setPass(rec.getString("pass"));
				U.setTel(rec.getString("tel"));
				U.setSex(rec.getString("sex"));
				U.setAge(rec.getString("age"));
				U.setAddress(rec.getString("address"));
				U.setPicPath(rec.getString("picture_path"));
				ListUser.add(U);

			}// end while
		}// end try
		catch (Exception e) {
			e.printStackTrace();
		}// end catch
		try {
			if (ps != null) {
				ps.close();
				db.ConDB().close();
			}// end if
		}// end try
		catch (SQLException e) {
			e.printStackTrace();
		}// end catch
		return ListUser;
	}// end method ManagetSearch

	public boolean ManagerEdit(UserModel userModel, String id) {
		ConDB db = new ConDB();
		PreparedStatement ps = null;
		String sql = "";
		boolean result = false;
		Connection con = null;

		try {
			con = db.ConDB();
			con.setAutoCommit(false);

			if (id != null && userModel.getID() == null) {
				System.out.println("se.getID() = " + id);
				sql = "SELECT * FROM tbl_md_user WHERE id = ? ";

				ps = con.prepareStatement(sql);
				ps.setString(1, id);

				ResultSet rec = ps.executeQuery();
				while ((rec != null) && (rec.next())) {
					userModel.setID(rec.getString("id"));
					userModel.setPass(rec.getString("pass"));
					userModel.setTel(rec.getString("tel"));
					userModel.setSex(rec.getString("sex"));
					userModel.setAge(rec.getString("age"));
					userModel.setAddress(rec.getString("address"));
					userModel.setPicPath(rec.getString("picture_path"));

				}// end while
			}// end if
			else if (id != null && userModel.getID() != null) {
				sql = "UPDATE tbl_md_user SET id = ? , pass = ? , tel = ? , sex = ? , age = ? , address = ? WHERE id = ?";
				ps = con.prepareStatement(sql);
				ps.setString(1, userModel.getID());
				ps.setString(2, userModel.getPass());
				ps.setString(3, userModel.getTel());
				ps.setString(4, userModel.getSex());
				ps.setString(5, userModel.getAge());
				ps.setString(6, userModel.getAddress());
				ps.setString(7, id);
				ps.executeUpdate();

				if (userModel.getPicPath() != "") {
					sql = "UPDATE tbl_md_user SET picture_path = ? WHERE id = ?";
					ps = con.prepareStatement(sql);
					ps.setString(1, userModel.getPicPath());
					ps.setString(2, userModel.getID());
					ps.executeUpdate();
				}
				con.commit();
				result = true;
			}// end if
		}// end try
		catch (Exception se) {
			// Handle errors for JDBC
			se.printStackTrace();
			// If there is an error then rollback the changes.
			System.out.println("Todo(186) : Rolling back data here....");
			try {
				if (con != null) {
					con.rollback();
					result = false;
				}// end try
				System.out.println("JDBC Transaction rolled back successfully");
			} catch (SQLException se2) {
				System.out.println("Todo(194) : SQLException in rollback = "
						+ se.getMessage());

			}// end catch
		}// end catch
		try {
			if (ps != null) {
				ps.close();
				db.ConDB().close();
			}// end if
		}// end try
		catch (SQLException e) {
			e.printStackTrace();
		}// end catch
		return result;
	}// end method ManagerEdit

	public boolean ManagerDelete(String id) {
		ConDB db = new ConDB();
		Connection con = null;
		PreparedStatement ps = null;
		String sql = "";
		boolean result = false;

		try {
			con = db.ConDB();
			con.setAutoCommit(false);

			sql = "DELETE FROM tbl_md_user WHERE id = ? ";
			ps = con.prepareStatement(sql);
			ps.setString(1, id);
			ps.executeUpdate();

			con.commit();
			result = true;

		}// end try
		catch (Exception se) {
			// Handle errors for JDBC
			se.printStackTrace();
			// If there is an error then rollback the changes.
			System.out.println("Todo(235) : Rolling back data here....");
			try {
				if (con != null) {
					con.rollback();
					result = false;
				}// end try
				System.out.println("JDBC Transaction rolled back successfully");
			} catch (SQLException se2) {
				System.out.println("Todo(243) : SQLException in rollback = "
						+ se.getMessage());

			}// end catch

		}// end catch
		try {
			if (ps != null) {
				ps.close();
				db.ConDB().close();
			}// end if
		}// end try
		catch (SQLException e) {
			e.printStackTrace();
		}// end catch
		return result;
	}// end method ManagerDelete

	public Boolean ManagetReportAdd(ImgModel imgModel) {
		Boolean result = false;
		ConDB db = new ConDB();
		PreparedStatement ps = null;
		String sql = "";
		Connection con = null;

		try {
			con = db.ConDB();
			con.setAutoCommit(false);

			sql = "INSERT INTO tbl_rd_report_inf (report_name,report_desc,status,create_date) VALUE (?,?,?,?)";
			ps = con.prepareStatement(sql,
					PreparedStatement.RETURN_GENERATED_KEYS);

			ps.setString(1, imgModel.getReportName());
			ps.setString(2, imgModel.getReportDesc());
			ps.setInt(3, imgModel.getReportStatus());
			ps.setString(4, imgModel.getDate());

			int count = ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			int infId = 0;
			if (rs.next()) {
				infId = rs.getInt(1);
			}// end get id of insert
			System.out.println("Inserted record's ID: " + infId);
			ps.close();

			if (imgModel.getListDetail().size() > 0) {
				for (int i = 0; i < imgModel.getListDetail().size(); i++) {
					ImgDetailModel listDetail = imgModel.getListDetail().get(i);

					sql = "INSERT INTO tbl_rd_report_txn (inf_id,title_img,desc_img,img_path,status_img,create_date) VALUE (?,?,?,?,?,?)";
					ps = con.prepareStatement(sql);

					ps.setInt(1, infId);
					ps.setString(2, listDetail.getTitleImg());
					ps.setString(3, listDetail.getDescImg());
					ps.setString(4, listDetail.getImgPath());
					ps.setInt(5, listDetail.getStatusImg());
					ps.setString(6, imgModel.getDate());
					ps.executeUpdate();
					ps.close();

				}
			}

			if (count > 0) {
				con.commit();
				result = true;
				// System.out.println("insert successfully");
			}

		}// end try
		catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
			// If there is an error then rollback the changes.
			System.out.println("Todo(320) : Rolling back data here....");
			try {
				if (con != null) {
					con.rollback();
					result = false;
				}// end try
				System.out.println("JDBC Transaction rolled back successfully");
			} catch (SQLException se2) {
				System.out.println("Todo(328) : SQLException in rollback = "
						+ se.getMessage());

			}// end catch

		}// end catch
		finally {
			try {
				if (ps != null) {
					ps.close();
					con.close();
				}// end if
			}// end try
			catch (SQLException e) {
				e.printStackTrace();
			}// end catch
		}// end finally
		return result;
		// https://www.journaldev.com/2483/java-jdbc-transaction-management-savepoint
		// Transaction Commit & Rollback
	}// end method ManagetAdd

	public List<ImgModel> ManagetReportSearch() {
		ConDB db = new ConDB();
		PreparedStatement ps = null;
		String sql = "";
		List<ImgModel> listModel = new ArrayList<ImgModel>();
		List<ImgDetailModel> listDetail = new ArrayList<ImgDetailModel>();

		try {
			sql = "SELECT * FROM tbl_rd_report_inf INNER JOIN tbl_rd_report_txn ON tbl_rd_report_inf.inf_id = tbl_rd_report_txn.inf_id";
			ps = db.ConDB().prepareStatement(sql);

			ResultSet rec = ps.executeQuery();
			while ((rec != null) && (rec.next())) {
				ImgModel imgHeadModel = new ImgModel();

				imgHeadModel.setInfId(rec.getInt("inf_id"));
				imgHeadModel.setReportName(rec.getString("report_name"));
				imgHeadModel.setReportDesc(rec.getString("report_desc"));
				imgHeadModel.setReportStatus(rec.getInt("status"));

				ImgDetailModel imgDetailModel = new ImgDetailModel();
				imgDetailModel.setRefInfId(rec
						.getInt("tbl_rd_report_txn.inf_id"));
				imgDetailModel.setTitleImg(rec.getString("title_img"));
				imgDetailModel.setDescImg(rec.getString("desc_img"));
				imgDetailModel.setImgPath(rec.getString("img_path"));
				imgDetailModel.setStatusImg(rec.getInt("status_img"));
				listDetail.add(imgDetailModel);
				imgHeadModel.setListDetail(listDetail);

				listModel.add(imgHeadModel);

			}// end while
		}// end try
		catch (Exception e) {
			e.printStackTrace();
		}// end catch
		try {
			if (ps != null) {
				ps.close();
				db.ConDB().close();
			}// end if
		}// end try
		catch (SQLException e) {
			e.printStackTrace();
		}// end catch
		return listModel;
		// https://www.w3schools.com/sql/sql_join_inner.asp
		// SQL inner join
	}// end method ManagetSearch

	public boolean ManagerReportDelete(String infid,
			List<ImgDetailModel> listDetail) {
		ConDB db = new ConDB();
		PreparedStatement ps = null;
		String sqlDelete = "";
		String sqlSearch = "";
		boolean result = false;
		Connection con = null;

		try {
			con = db.ConDB();
			con.setAutoCommit(false);

			sqlSearch = "SELECT * FROM tbl_rd_report_txn WHERE inf_id = ? ";
			ps = con.prepareStatement(sqlSearch);
			ps.setString(1, infid);
			ResultSet rec = ps.executeQuery();
			while ((rec != null) && (rec.next())) {
				ImgDetailModel imgDetailModel = new ImgDetailModel();
				imgDetailModel.setImgPath(rec.getString("img_path"));
				listDetail.add(imgDetailModel);

			}// end while

			sqlDelete = "DELETE FROM tbl_rd_report_txn WHERE inf_id = ? ";
			ps = con.prepareStatement(sqlDelete);
			ps.setString(1, infid);
			ps.executeUpdate();

			sqlDelete = "DELETE FROM tbl_rd_report_inf WHERE inf_id = ? ";
			ps = con.prepareStatement(sqlDelete);
			ps.setString(1, infid);
			ps.executeUpdate();

			con.commit();
			result = true;

		}// end try
		catch (Exception e) {
			e.printStackTrace();

			try {
				if (con != null) {
					con.rollback();
					result = false;
				}
				System.out.println("JDBC Transaction rolled back successfully");
			} catch (SQLException se2) {
				System.out.println("Todo(449) : SQLException in rollback = "
						+ e.getMessage());
			}// end try

		}// end catch
		try {
			if (ps != null) {
				ps.close();
				db.ConDB().close();
			}// end if
		}// end try
		catch (SQLException e) {
			e.printStackTrace();
		}// end catch
		return result;
		// https://stackoverflow.com/questions/1233451/delete-from-two-tables-in-one-query
		// https://www.w3schools.com/sql/sql_join_inner.asp
	}

	public boolean ManagerReportDetailDelete(String txnId,String imgPath) {
		ConDB db = new ConDB();
		PreparedStatement ps = null;
		String sqlSearch = "";
		String sqlDelete = "";
		boolean result = false;
		Connection con = null;

		try {
			con = db.ConDB();
			con.setAutoCommit(false);
			
			sqlSearch = "SELECT * FROM tbl_rd_report_txn WHERE txn_id = ? ";
			ps = con.prepareStatement(sqlSearch);
			ps.setString(1, txnId);
			ResultSet rec = ps.executeQuery();
			while ((rec != null) && (rec.next())) {
				imgPath = rec.getString("img_path");
				System.out.println("Todo(486) imgPath = "+imgPath);
			}// end while
			

			sqlDelete = "DELETE FROM tbl_rd_report_txn WHERE txn_id = ? ";
			ps = con.prepareStatement(sqlDelete);
			ps.setString(1, txnId);
			ps.executeUpdate();


			con.commit();
			result = true;

		}// end try
		catch (Exception e) {
			e.printStackTrace();

			try {
				if (con != null) {
					con.rollback();
					result = false;
				}
				System.out.println("JDBC Transaction rolled back successfully");
			} catch (SQLException se2) {
				System.out.println("Todo(499) : SQLException in rollback = "
						+ e.getMessage());
			}// end try

		}// end catch
		try {
			if (ps != null) {
				ps.close();
				db.ConDB().close();
			}// end if
		}// end try
		catch (SQLException e) {
			e.printStackTrace();
		}// end catch
		return result;
		// https://stackoverflow.com/questions/1233451/delete-from-two-tables-in-one-query
		// https://www.w3schools.com/sql/sql_join_inner.asp
	}
	
	public boolean ManagerReportEdit(String infid, ImgModel imgModel) {

		Boolean result = false;
		ConDB db = new ConDB();
		PreparedStatement ps = null;
		String sql = "";
		Connection con = db.ConDB();

		// List<ImgModel> listModel = new ArrayList<ImgModel>();

		try {
			con.setAutoCommit(false);

			if (infid != null && imgModel.getReportName() != null) {

				sql = "UPDATE tbl_rd_report_inf SET report_name = ? , report_desc = ? , status = ? , update_date = ? WHERE inf_id = ?";
				ps = con.prepareStatement(sql);
				ps.setString(1, imgModel.getReportName());
				ps.setString(2, imgModel.getReportDesc());
				ps.setInt(3, imgModel.getReportStatus());
				ps.setString(4, imgModel.getDate());
				ps.setString(5, infid);
				int count = ps.executeUpdate();
				ps.close();

				if (imgModel.getListDetail().size() > 0) {
					for (int i = 0; i < imgModel.getListDetail().size(); i++) {
						ImgDetailModel listDetailModel = imgModel
								.getListDetail().get(i);

						if (listDetailModel.getImgPath() != "") {
							sql = "UPDATE tbl_rd_report_txn SET title_img = ? , desc_img = ? , img_path = ? , status_img = ? , update_date = ? WHERE txn_id = ?";
							ps = con.prepareStatement(sql);

							System.out.println("Todo(454): Tnx_ID " + i + " : "
									+ listDetailModel.getRefInfId());
							ps.setString(1, listDetailModel.getTitleImg());
							ps.setString(2, listDetailModel.getDescImg());
							ps.setString(3, listDetailModel.getImgPath());
							ps.setInt(4, listDetailModel.getStatusImg());
							ps.setString(5, imgModel.getDate());
							ps.setInt(6, listDetailModel.getRefInfId());
							count = ps.executeUpdate();
							ps.close();

						} else {

							sql = "UPDATE tbl_rd_report_txn SET title_img = ? , desc_img = ? , status_img = ? , update_date = ? WHERE txn_id = ?";
							ps = con.prepareStatement(sql);

							System.out.println("Todo(454): Tnx_ID " + i + " : "
									+ listDetailModel.getRefInfId());
							ps.setString(1, listDetailModel.getTitleImg());
							ps.setString(2, listDetailModel.getDescImg());
							ps.setInt(3, listDetailModel.getStatusImg());
							ps.setString(4, imgModel.getDate());
							ps.setInt(5, listDetailModel.getRefInfId());
							count = ps.executeUpdate();
							ps.close();

						}

					}
				}// End if
				if (count > 0) {
					result = true;
					con.commit();
					// System.out.println("insert successfully");
				}// End if

			}// end else if

		}// end try
		catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
			// If there is an error then rollback the changes.

			try {
				if (con != null) {
					result = false;
					con.rollback();
				}
				System.out.println("JDBC Transaction rolled back successfully");
			} catch (SQLException se2) {
				System.out.println("Todo(603) : SQLException in rollback = "
						+ se.getMessage());

			}// end catch

		}// end catch
		finally {
			try {
				if (ps != null) {
					ps.close();
					con.close();
				}// end if
			}// end try
			catch (SQLException e) {
				e.printStackTrace();
			}// end catch
		}// end finally

		return result;
	}// end method ManagerReportEdit

	public ImgModel ManagerReportEditSearch(String infid, ImgModel imgModel) {

		ConDB db = new ConDB();
		PreparedStatement ps = null;
		String sql = "";
		Connection con = db.ConDB();

		List<ImgDetailModel> listDetail = new ArrayList<ImgDetailModel>();

		try {
			if (infid != null && imgModel.getReportName() == null) {
				sql = "SELECT * FROM tbl_rd_report_inf INNER JOIN tbl_rd_report_txn ON tbl_rd_report_inf.inf_id = tbl_rd_report_txn.inf_id and tbl_rd_report_inf.inf_id = ?";
				ps = con.prepareStatement(sql);
				ps.setString(1, infid);

				ResultSet rec = ps.executeQuery();
				while ((rec != null) && (rec.next())) {

					if (rec.getInt("inf_id") != imgModel.getInfId()) {
						imgModel.setInfId(rec.getInt("inf_id"));
						imgModel.setReportName(rec.getString("report_name"));
						imgModel.setReportDesc(rec.getString("report_desc"));
						imgModel.setReportStatus(rec.getInt("status"));
					}

					ImgDetailModel imgDetailModel = new ImgDetailModel();
					imgDetailModel.setRefInfId(rec.getInt("txn_id"));
					imgDetailModel.setTitleImg(rec.getString("title_img"));
					imgDetailModel.setDescImg(rec.getString("desc_img"));
					imgDetailModel.setImgPath(rec.getString("img_path"));
					imgDetailModel.setStatusImg(rec.getInt("status_img"));
					listDetail.add(imgDetailModel);
					imgModel.setListDetail(listDetail);

				}// end while

			}// end if

		}// end try
		catch (Exception e) {
			e.printStackTrace();
		}// end try

		return imgModel;
	}// end method ManagerReportEditSearch

}// end class

