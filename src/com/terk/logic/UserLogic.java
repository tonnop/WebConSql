package com.terk.logic;

import java.io.InputStream;
import java.sql.ResultSet;
import java.util.*;

import com.login.model.ImgDetailModel;
import com.login.model.ImgModel;
import com.login.model.UserModel;
import com.terk.manage.LoginManage;
import com.terk.manage.UserManager;

public class UserLogic {

	private UserManager userManager = new UserManager();

	public boolean Useradd(UserModel userModel) {
		boolean result = userManager.ManagetAdd(userModel);
		return result;
	}// end Useradd

	public List<UserModel> Usersearch(UserModel userModel, UserModel se) {
		UserManager sh = new UserManager();
		List<UserModel> listResult = sh.ManagetSearch(userModel, se);

		return listResult;
	}// end Usersearch

	public boolean Useredit(UserModel userModel, String id) {
		UserManager ed = new UserManager();
		boolean result = ed.ManagerEdit(userModel, id);

		return result;
	}// end Useredit

	public boolean Userdelete(String id) {
		UserManager de = new UserManager();
		boolean result = de.ManagerDelete(id);

		return result;
	}// end Userdelete

	public boolean ReportAdd(ImgModel imgModel) {
		UserManager sh = new UserManager();
		Boolean result = sh.ManagetReportAdd(imgModel);

		return result;
	}// end ReportAdd
	
	public List<ImgModel> ReportSearch() {
		UserManager sh = new UserManager();
		List<ImgModel> listModel = sh.ManagetReportSearch();

		return listModel;
	}// end ReportSearch
	
	public ImgModel ReportEditSearch(String infid , ImgModel imgModel) {
		UserManager ed = new UserManager();
		ed.ManagerReportEditSearch(infid ,imgModel);

		return imgModel;
	}// end ReportEditSearch
	
	public boolean ReportEdit(String infid , ImgModel imgModel) {
		UserManager ed = new UserManager();
		boolean result = ed.ManagerReportEdit(infid ,imgModel);

		return result;
	}// end Useredit
	
	public boolean Reportdelete(String infid , List<ImgDetailModel> listDetail) {
		UserManager de = new UserManager();
		boolean result = de.ManagerReportDelete(infid ,listDetail);

		return result;
	}// end Userdelete
	
	public boolean ReportDetailDelete(String txnId,String imgPath) {
		UserManager de = new UserManager();
		boolean result = de.ManagerReportDetailDelete(txnId, imgPath);

		return result;
	}// end Userdelete
	
	public boolean LoginLogic(String id,String pass){
		LoginManage loginManage = new LoginManage();
		boolean result = loginManage.LoginUser(id, pass);
		
		return result;
	}
	
	public boolean ProfileSearch(UserModel userModel){
		LoginManage loginManage = new LoginManage();
		boolean result = loginManage.ProflieSearchManage(userModel);
		
		return result;
	}


}